﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static bool GameIsOver;

    public GameObject gameOverUI;
    public GameObject completeLevelUI;

    public static bool takenScreenshot = false;
    public static float enemyKillStrick = 0;

    void Start()
    {
        GameIsOver = false;
        WaveSpawner.EnemiesAlive = 0;

        if (SceneManager.GetActiveScene().name == "MainLevel2")
        {
            PlayerStats.Rounds = 0;
        }

        StartCoroutine(KillStrickDecrease());
    }

    // Update is called once per frame
    void Update () {
        if (GameIsOver)
            return;

		if (PlayerStats.Lives <= 0)
        {
            EndGame();
        }

        if (Input.GetKey(KeyCode.Y))
            WinLevel();

        CheckKillStrick();
        enemyKillStrick = Mathf.Clamp(enemyKillStrick, 0, Mathf.Infinity);

	}

    void EndGame()
    {
        GameIsOver = true;
        gameOverUI.SetActive(true);
    }

    public void WinLevel()
    {
        GameIsOver = true;
        completeLevelUI.SetActive(true);

        if (SceneManager.GetActiveScene().name == "MainLevel")
        {
            PlayerPrefs.SetInt("Save", 1);
            Debug.Log("saved");
        }
    }

    void CheckKillStrick()
    {
        if (enemyKillStrick >= 3)
        {
            takenScreenshot = true;
            enemyKillStrick = 0;
        }
    }

    IEnumerator KillStrickDecrease()
    {
        yield return new WaitForSeconds(3f);
        enemyKillStrick--;

        StartCoroutine(KillStrickDecrease());
    }

}
