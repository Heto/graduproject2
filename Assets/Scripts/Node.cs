﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour {

    public Color hoverColor;
    public Color notEnoughMoneyColor;
    public Vector3 positionOffset;

    //[HideInInspector]
    public GameObject turret;
    [HideInInspector]
    public TurretBlueprint turretBlueprint;
    [HideInInspector]
    public bool isUpgraded = false;

    private Renderer rend;
    private Color startColor;

    BuildManager buildManager;

    public Turret targetTurret;
    public float nodeLevel;
    private bool upgraded = false;

    public int cost;
    public float dmg;
    public float aspd;
    public float range;
    public float crichan;
    public float cridmg;
    public float stunRange;
    public float stunTime;
    public float radius;
    public float mpGain;
    public float skillDmg;
    public float skillRadius;
    public float supporterDmg;
    public float dmgBuff;
    public float asBuff;
    public float slowAmount;

    public Sprite adasUI;
    public Sprite rangeUI;
    public Sprite crichanUI;
    public Sprite cridmgUI;
    public Sprite crisquUI;
    public Sprite asUI;
    public Sprite assquUI;
    public Sprite dmgUI;
    public Sprite dmgsquUI;
    public Sprite runanUI;
    public Sprite rangerUI;
    public Sprite executionerUI;
    public Sprite radiusUI;
    public Sprite mpgainUI;
    public Sprite allsquUI;
    public Sprite skillUI;
    public Sprite sorcererUI;
    public Sprite summonerUI;
    public Sprite buffdmgUI;
    public Sprite buffasUI;
    public Sprite slowamountUI;
    public Sprite bufferUI;
    public Sprite debufferUI;
    public Sprite stuntimeUI;
    public Sprite stuntimesquUI;
    public Sprite stunnumUI;
    public Sprite knightUI;
    public Sprite berserkerUI;

    //Archer
    private int crichanNum = 0;
    private int cridmgNum = 0;
    private int asNum = 0;
    private int dmgNum = 0;
    private bool beforeNormal6 = false;

    //Magician
    private bool beforeRadius4 = false;
    private bool beforeMpgain4 = false;
    private bool Mpgain4On = false;
    private bool takeBottomLine_2 = false;
    private bool range2On = false;
    private bool beforeNormal3 = false;

    private int mpgainNum = 0;
    private int asNum_2 = 0;
    private int radiusNum = 0;
    private int skillNum = 0;

    //Melee
    private bool beforeNormal3_0 = false;
    private bool beforeNormal6_0 = false;

    private int stunnum1Num = 0;
    private int stunnum3Num = 0;
    private int upside1Num = 0;
    private int downside1Num = 0;
    private int upside2Num = 0;
    private int downside2Num = 0;
    private int middleNum = 0;


    //Supporter
    private bool beforeNormal3_3 = false;
    private bool beforeNormal6_3 = false;

    private int up1Num_3 = 0;
    private int middle1Num_3 = 0;
    private int down1Num_3 = 0;
    private int centerNum = 0;
    private int up2Num_3 = 0;
    private int down2Num_3 = 0;
    private int centerupNum = 0;
    private int centerdownNum = 0;

    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;

        buildManager = BuildManager.instance;
        nodeLevel = 0;
    }

    public Vector3 GetBuildPosition()
    {
        return transform.position + positionOffset;
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
               
        if (turret != null)
        {
            targetTurret = turret.GetComponent<Turret>();
            buildManager.SelectNode(this);

            if (TutorialManager.tutorialProgress == 5)
                TutorialManager.tutorialProgress = 6;
            return;
        }

        if (!buildManager.CanBuild)
            return;

        BuildTurret(buildManager.GetTurretToBuild());
        if (TutorialManager.tutorialProgress == 4)
            TutorialManager.tutorialProgress = 5;
    }

    void BuildTurret (TurretBlueprint blueprint)
    {
        if (PlayerStats.Money < blueprint.cost)
        {
            Debug.Log("Not enough money to build that!");
            return;
        }

        PlayerStats.Money -= blueprint.cost;

        GameObject _turret = (GameObject)Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;
        targetTurret = _turret.GetComponent<Turret>();

        turretBlueprint = blueprint;

        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

    }

    public void UpgradeTurret()
    {
        if (PlayerStats.Money < turretBlueprint.upgradeCost)
        {
            Debug.Log("Not enough money to upgrade that!");
            return;
        }

        PlayerStats.Money -= turretBlueprint.upgradeCost;

        Destroy(turret);

        GameObject _turret = (GameObject)Instantiate(turretBlueprint.upgradedPrefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;
        
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        isUpgraded = true;

        Debug.Log("Turret upgraded!");
    }

    public void SellTurret()
    {
        PlayerStats.Money += turretBlueprint.GetSellAmount();

        GameObject effect = (GameObject)Instantiate(buildManager.sellEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        Destroy(turret);
        turretBlueprint = null;
    }

    void OnMouseEnter()
    {
        if (targetTurret)
            targetTurret.rangeObject.SetActive(true);

        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (!buildManager.CanBuild)
            return;

        if (buildManager.HasMoney)
        {
            rend.material.color = hoverColor;
        } else
        {
            rend.material.color = notEnoughMoneyColor;
        }
        
    }

    void OnMouseExit()
    {
        rend.material.color = startColor;

        if (targetTurret)
            targetTurret.rangeObject.SetActive(false);
    }

    #region Melee
    public void Normal0_0()
    {
        if (nodeLevel < 0)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal0").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal0").GetComponent<Button>().enabled = false;
    }
    public void Normal1_0()
    {
        if (nodeLevel < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal1").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal1").GetComponent<Button>().enabled = false;
    }
    public void Normal9_0()
    {
        if (nodeLevel < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        stunnum1Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal9").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal9").GetComponent<Button>().enabled = false;
    }
    public void Normal10_0()
    {
        if (stunnum1Num < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        stunnum1Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal10").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal10").GetComponent<Button>().enabled = false;
    }
    public void Stunnum1_0()
    {
        if (stunnum1Num < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.nearEnemyStunRange += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("stunnum1").GetComponent<Image>().sprite = stunnumUI;
        GameObject.Find("stunnum1").GetComponent<Button>().enabled = false;
    }
    public void Normal2_0()
    {
        if (nodeLevel < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal2").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal2").GetComponent<Button>().enabled = false;
    }
    public void Range1_0()
    {
        if (nodeLevel < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        upside1Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 2f;
        PlayerStats.Money -= 20;
        GameObject.Find("range1").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range1").GetComponent<Button>().enabled = false;
    }
    public void Stuntime1_0()
    {
        if (nodeLevel < 4 || upside1Num < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        upside1Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.stunTime += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("stuntime1").GetComponent<Image>().sprite = stuntimeUI;
        GameObject.Find("stuntime1").GetComponent<Button>().enabled = false;
    }
    public void Stuntime2_0()
    {
        if (nodeLevel < 5 || upside1Num < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        upside1Num++;
        beforeNormal3_0 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.stunTime += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("stuntime2").GetComponent<Image>().sprite = stuntimeUI;
        GameObject.Find("stuntime2").GetComponent<Button>().enabled = false;
    }
    public void As1_0()
    {
        if (nodeLevel < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        downside1Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.1f;
        PlayerStats.Money -= 20;
        GameObject.Find("as1").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as1").GetComponent<Button>().enabled = false;
    }
    public void As2_0()
    {
        if (nodeLevel < 4 || downside1Num < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        downside1Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.1f;
        PlayerStats.Money -= 20;
        GameObject.Find("as2").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as2").GetComponent<Button>().enabled = false;
    }
    public void As3_0()
    {
        if (nodeLevel < 5 || downside1Num < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        downside1Num++;
        beforeNormal3_0 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.1f;
        PlayerStats.Money -= 20;
        GameObject.Find("as3").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as3").GetComponent<Button>().enabled = false;
    }
    public void Normal3_0()
    {
        if (!beforeNormal3_0)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        middleNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal3").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal3").GetComponent<Button>().enabled = false;
    }
    public void Normal4_0()
    {
        if (middleNum < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        middleNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal4").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal4").GetComponent<Button>().enabled = false;
    }
    public void Normal5_0()
    {
        if (middleNum < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        middleNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal5").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal5").GetComponent<Button>().enabled = false;
    }
    public void Range2_0()
    {
        if (middleNum < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        upside2Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("range2").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range2").GetComponent<Button>().enabled = false;
    }
    public void Stuntime3_0()
    {
        if (upside2Num < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        upside2Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.stunTime += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("stuntime3").GetComponent<Image>().sprite = stuntimeUI;
        GameObject.Find("stuntime3").GetComponent<Button>().enabled = false;
    }
    public void Stuntime4_0()
    {
        if (upside2Num < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        upside2Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.stunTime += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("stuntime4").GetComponent<Image>().sprite = stuntimeUI;
        GameObject.Find("stuntime4").GetComponent<Button>().enabled = false;
    }
    public void StuntimeSqu_0()
    {
        if (upside1Num < 3 || upside2Num < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        upside2Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.stunTime += 0.4f;
        PlayerStats.Money -= 20;
        GameObject.Find("stuntimeSqu").GetComponent<Image>().sprite = stuntimesquUI;
        GameObject.Find("stuntimeSqu").GetComponent<Button>().enabled = false;
    }
    public void Stunnum2_0()
    {
        if (upside2Num < 4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        upside2Num++;
        beforeNormal6_0 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.nearEnemyStunRange += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("stunnum2").GetComponent<Image>().sprite = stunnumUI;
        GameObject.Find("stunnum2").GetComponent<Button>().enabled = false;
    }
    public void As4_0()
    {
        if (middleNum < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        downside2Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.1f;
        PlayerStats.Money -= 20;
        GameObject.Find("as4").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as4").GetComponent<Button>().enabled = false;
    }
    public void As5_0()
    {
        if (downside2Num < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        downside2Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.1f;
        PlayerStats.Money -= 20;
        GameObject.Find("as5").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as5").GetComponent<Button>().enabled = false;
    }
    public void Dmg1_0()
    {
        if (downside2Num < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        downside2Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 3f;
        PlayerStats.Money -= 20;
        GameObject.Find("dmg1").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("dmg1").GetComponent<Button>().enabled = false;
    }
    public void Dmg2_0()
    {
        if (downside2Num < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        downside2Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 3f;
        PlayerStats.Money -= 20;
        GameObject.Find("dmg2").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("dmg2").GetComponent<Button>().enabled = false;
    }
    public void DmgSqu_0()
    {
        if (downside2Num < 4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        downside2Num++;
        beforeNormal6_0 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 6f;
        PlayerStats.Money -= 20;
        GameObject.Find("dmgSqu").GetComponent<Image>().sprite = dmgsquUI;
        GameObject.Find("dmgSqu").GetComponent<Button>().enabled = false;
    }
    public void Normal6_0()
    {
        if (!beforeNormal6_0)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        middleNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal6").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal6").GetComponent<Button>().enabled = false;
    }
    public void Normal7_0()
    {
        if (middleNum < 4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        middleNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal7").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal7").GetComponent<Button>().enabled = false;
    }
    public void Normal11_0()
    {
        if (middleNum < 5)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        stunnum3Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal11").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal11").GetComponent<Button>().enabled = false;
    }
    public void Normal12_0()
    {
        if (stunnum3Num < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        stunnum3Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal12").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal12").GetComponent<Button>().enabled = false;
    }
    public void Normal13_0()
    {
        if (stunnum3Num < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        stunnum3Num++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal13").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal13").GetComponent<Button>().enabled = false;
    }
    public void Stunnum3_0()
    {
        if (stunnum3Num < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.nearEnemyStunRange += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("stunnum3").GetComponent<Image>().sprite = stunnumUI;
        GameObject.Find("stunnum3").GetComponent<Button>().enabled = false;
    }
    public void Normal8_0()
    {
        if (middleNum < 5)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        middleNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 1.5f;
        targetTurret.fireRate += 0.035f;
        PlayerStats.Money -= 20;
        GameObject.Find("normal8").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal8").GetComponent<Button>().enabled = false;
    }
    public void KnightUpgrade()
    {
        if (middleNum < 6 || upgraded)
        {
            return;
        }
        if (PlayerStats.Money < 40)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.knightUP = true;
        upgraded = true;
        PlayerStats.Money -= 40;
        GameObject.Find("knight").GetComponent<Image>().sprite = knightUI;
        GameObject.Find("knight").GetComponent<Button>().enabled = false;
    }
    public void BerserkerUpgrade()
    {
        if (middleNum < 6 || upgraded)
        {
            return;
        }
        if (PlayerStats.Money < 40)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.berserkerUP = true;
        targetTurret.berserkerSkill = true;
        upgraded = true;
        PlayerStats.Money -= 40;
        GameObject.Find("berserker").GetComponent<Image>().sprite = berserkerUI;
        GameObject.Find("berserker").GetComponent<Button>().enabled = false;
    }
    #endregion

    #region Archer
    public void Normal0()
    {
        if (nodeLevel < 0)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.05f;
        targetTurret.turretDmg += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal0").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal0").GetComponent<Button>().enabled = false;
    }
    public void Normal1()
    {
        if (nodeLevel < 1)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.05f;
        targetTurret.turretDmg += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal1").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal1").GetComponent<Button>().enabled = false;
    }
    public void Normal2()
    {
        if (nodeLevel < 2)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.05f;
        targetTurret.turretDmg += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal2").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal2").GetComponent<Button>().enabled = false;
    }
    public void Normal3()
    {
        if (nodeLevel < 3)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.05f;
        targetTurret.turretDmg += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal3").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal3").GetComponent<Button>().enabled = false;
    }
    public void Range1()
    {
        if (nodeLevel < 4)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1.5f;
        PlayerStats.Money -= 20;
        GameObject.Find("range1").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range1").GetComponent<Button>().enabled = false;
    }
    public void Range2()
    {
        if (nodeLevel < 4.001f)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1.5f;
        PlayerStats.Money -= 20;
        GameObject.Find("range2").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range2").GetComponent<Button>().enabled = false;
    }
    public void Range3()
    {
        if (nodeLevel < 4.002f)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 2f;
        PlayerStats.Money -= 20;
        GameObject.Find("range3").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range3").GetComponent<Button>().enabled = false;
    }
    public void Normal4()
    {
        if (nodeLevel < 4)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.05f;
        targetTurret.turretDmg += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal4").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal4").GetComponent<Button>().enabled = false;
    }
    public void Crichan1()
    {
        if (nodeLevel < 5)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        crichanNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.criticalChance += 0.1f;
        PlayerStats.Money -= 20;
        GameObject.Find("crichan1").GetComponent<Image>().sprite = crichanUI;
        GameObject.Find("crichan1").GetComponent<Button>().enabled = false;
    }
    public void Crichan2()
    {
        if (nodeLevel < 5.001f)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        crichanNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.criticalChance += 0.1f;
        PlayerStats.Money -= 20;
        GameObject.Find("crichan2").GetComponent<Image>().sprite = crichanUI;
        GameObject.Find("crichan2").GetComponent<Button>().enabled = false;
    }
    public void Crichan3()
    {
        if (nodeLevel < 5.002f || crichanNum < 2)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        crichanNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.criticalChance += 0.1f;
        PlayerStats.Money -= 20;
        GameObject.Find("crichan3").GetComponent<Image>().sprite = crichanUI;
        GameObject.Find("crichan3").GetComponent<Button>().enabled = false;
    }
    public void Cridmg1()
    {
        if (nodeLevel < 5.001f)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        cridmgNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.criticalDmg += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("cridmg1").GetComponent<Image>().sprite = cridmgUI;
        GameObject.Find("cridmg1").GetComponent<Button>().enabled = false;
    }
    public void Cridmg2()
    {
        if (nodeLevel < 5.002f || cridmgNum < 1)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        cridmgNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.criticalDmg += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("cridmg2").GetComponent<Image>().sprite = cridmgUI;
        GameObject.Find("cridmg2").GetComponent<Button>().enabled = false;
    }
    public void CriSqu()
    {
        if (crichanNum < 3 && cridmgNum < 2)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.criticalChance += 0.1f;
        targetTurret.criticalDmg += 0.2f;
        PlayerStats.Money -= 20;
        GameObject.Find("criSqu").GetComponent<Image>().sprite = crisquUI;
        GameObject.Find("criSqu").GetComponent<Button>().enabled = false;
    }
    public void Normal5()
    {
        if (nodeLevel < 5)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.05f;
        targetTurret.turretDmg += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal5").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal5").GetComponent<Button>().enabled = false;
    }
    public void As1()
    {
        if (nodeLevel < 6)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        asNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.2f;
        PlayerStats.Money -= 20;
        GameObject.Find("as1").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as1").GetComponent<Button>().enabled = false;
    }
    public void As2()
    {
        if (nodeLevel < 6.01f || asNum < 1)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        asNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.2f;
        PlayerStats.Money -= 20;
        GameObject.Find("as2").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as2").GetComponent<Button>().enabled = false;
    }
    public void As3()
    {
        if (nodeLevel < 6.02f || asNum < 2)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        asNum++;
        beforeNormal6 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.2f;
        PlayerStats.Money -= 20;
        GameObject.Find("as3").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as3").GetComponent<Button>().enabled = false;
    }
    public void AsSqu()
    {
        if (asNum < 3)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.3f;
        PlayerStats.Money -= 20;
        GameObject.Find("asSqu").GetComponent<Image>().sprite = assquUI;
        GameObject.Find("asSqu").GetComponent<Button>().enabled = false;
    }
    public void Dmg1()
    {
        if (nodeLevel < 6)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        dmgNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 3;
        PlayerStats.Money -= 20;
        GameObject.Find("dmg1").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("dmg1").GetComponent<Button>().enabled = false;
    }
    public void Dmg2()
    {
        if (nodeLevel < 6.01f || dmgNum < 1)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        dmgNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 3;
        PlayerStats.Money -= 20;
        GameObject.Find("dmg2").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("dmg2").GetComponent<Button>().enabled = false;
    }
    public void Dmg3()
    {
        if (nodeLevel < 6.02f || dmgNum < 2)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        dmgNum++;
        beforeNormal6 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 3;
        PlayerStats.Money -= 20;
        GameObject.Find("dmg3").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("dmg3").GetComponent<Button>().enabled = false;
    }
    public void DmgSqu()
    {
        if (dmgNum < 3)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        dmgNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 5;
        PlayerStats.Money -= 20;
        GameObject.Find("dmgSqu").GetComponent<Image>().sprite = dmgsquUI;
        GameObject.Find("dmgSqu").GetComponent<Button>().enabled = false;
    }
    public void Normal6()
    {
        if (!beforeNormal6)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.05f;
        targetTurret.turretDmg += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal6").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal6").GetComponent<Button>().enabled = false;
    }
    public void Normal7()
    {
        if (nodeLevel < 7)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.08f;
        targetTurret.turretDmg += 4;
        PlayerStats.Money -= 20;
        GameObject.Find("normal7").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal7").GetComponent<Button>().enabled = false;
    }
    public void Runan1()
    {
        if (nodeLevel < 8)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.2f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.RunanActive(1);
        PlayerStats.Money -= 20;
        GameObject.Find("runan1").GetComponent<Image>().sprite = runanUI;
        GameObject.Find("runan1").GetComponent<Button>().enabled = false;
    }
    public void Runan2()
    {
        if (nodeLevel < 8.2f)
        {
            //메시지UI출력 : 이전 노드를 찍어야 합니다.
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.1f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.RunanActive(2);
        PlayerStats.Money -= 20;
        GameObject.Find("runan2").GetComponent<Image>().sprite = runanUI;
        GameObject.Find("runan2").GetComponent<Button>().enabled = false;
    }
    public void RangerUpgrade()
    {
        if (nodeLevel < 8 || upgraded)
        {
            return;
        }
        if (PlayerStats.Money < 40)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.rangerUP = true;
        upgraded = true;
        PlayerStats.Money -= 40;
        GameObject.Find("ranger").GetComponent<Image>().sprite = rangerUI;
        GameObject.Find("ranger").GetComponent<Button>().enabled = false;
    }
    public void ExecutionerUpgrade()
    {
        if (nodeLevel < 8 || upgraded)
        {
            return;
        }
        if (PlayerStats.Money < 40)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.executionerUP = true;
        upgraded = true;
        PlayerStats.Money -= 40;
        GameObject.Find("executioner").GetComponent<Image>().sprite = executionerUI;
        GameObject.Find("executioner").GetComponent<Button>().enabled = false;
    }
    #endregion

    #region Magician
    public void Normal0_2()
    {
        if (nodeLevel < 0)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 5;
        PlayerStats.Money -= 20;
        GameObject.Find("normal0").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal0").GetComponent<Button>().enabled = false;
    }
    public void Normal1_2()
    {
        if (nodeLevel < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 5;
        PlayerStats.Money -= 20;
        GameObject.Find("normal1").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal1").GetComponent<Button>().enabled = false;
    }
    public void Radius1_2()
    {
        if (nodeLevel < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        radiusNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.explosionRadius += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("radius1").GetComponent<Image>().sprite = radiusUI;
        GameObject.Find("radius1").GetComponent<Button>().enabled = false;
    }
    public void Radius2_2()
    {
        if (nodeLevel < 2.001f)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        beforeRadius4 = true;
        radiusNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.explosionRadius += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("radius2").GetComponent<Image>().sprite = radiusUI;
        GameObject.Find("radius2").GetComponent<Button>().enabled = false;
    }
    public void Radius3_2()
    {
        if (nodeLevel < 2.001f)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        beforeRadius4 = true;
        radiusNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.explosionRadius += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("radius3").GetComponent<Image>().sprite = radiusUI;
        GameObject.Find("radius3").GetComponent<Button>().enabled = false;
    }
    public void Radius4_2()
    {
        if (!beforeRadius4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.002f;
        radiusNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.explosionRadius += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("radius4").GetComponent<Image>().sprite = radiusUI;
        GameObject.Find("radius4").GetComponent<Button>().enabled = false;
    }
    public void Mpgain1_2()
    {
        if (nodeLevel < 2.002f)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        mpgainNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("mpgain1").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("mpgain1").GetComponent<Button>().enabled = false;
    }
    public void Mpgain2_2()
    {
        if (nodeLevel < 2.004f)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        mpgainNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("mpgain2").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("mpgain2").GetComponent<Button>().enabled = false;
    }
    public void Mpgain3_2()
    {
        if (nodeLevel < 2.004f)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        beforeMpgain4 = true;
        mpgainNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("mpgain3").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("mpgain3").GetComponent<Button>().enabled = false;
    }
    public void Mpgain4_2()
    {
        if (nodeLevel < 4 && !beforeMpgain4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        Mpgain4On = true;
        mpgainNum++;
        beforeNormal3 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1f;
        PlayerStats.Money -= 20;
        GameObject.Find("mpgain4").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("mpgain4").GetComponent<Button>().enabled = false;
    }
    public void As1_2()
    {
        if (!Mpgain4On)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        asNum_2++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("as1").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as1").GetComponent<Button>().enabled = false;
    }
    public void As2_2()
    {
        if (nodeLevel < 2.013f || !Mpgain4On)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        asNum_2++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("as2").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as2").GetComponent<Button>().enabled = false;
    }
    public void As3_2()
    {
        if (nodeLevel < 2.023f || !Mpgain4On)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.01f;
        asNum_2++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.fireRate += 0.15f;
        PlayerStats.Money -= 20;
        GameObject.Find("as3").GetComponent<Image>().sprite = asUI;
        GameObject.Find("as3").GetComponent<Button>().enabled = false;
    }
    public void AllSqu_2()
    {
        if (radiusNum < 4 && mpgainNum < 4 && asNum_2 < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.explosionRadius += 1f;
        targetTurret.mpGainAmount += 1f;
        targetTurret.fireRate += 0.2f;
        PlayerStats.Money -= 20;
        GameObject.Find("allSqu").GetComponent<Image>().sprite = allsquUI;
        GameObject.Find("allSqu").GetComponent<Button>().enabled = false;
    }
    public void Range1_2()
    {
        if (nodeLevel < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        takeBottomLine_2 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("range1").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range1").GetComponent<Button>().enabled = false;
    }
    public void Range2_2()
    {
        if (!takeBottomLine_2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        range2On = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("range2").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range2").GetComponent<Button>().enabled = false;
    }
    public void Range3_2()
    {
        if (nodeLevel < 2.001f || !takeBottomLine_2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.002f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("range3").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range3").GetComponent<Button>().enabled = false;
    }
    public void Range4_2()
    {
        if (nodeLevel < 2.003f && !range2On)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel += 0.001f;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("range4").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range4").GetComponent<Button>().enabled = false;
    }
    public void Skill1_2()
    {
        if (nodeLevel < 4 && !range2On)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        skillNum++;
        beforeNormal3 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.skillDmg += 12;
        targetTurret.skillExplosionRadius += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill1").GetComponent<Image>().sprite = skillUI;
        GameObject.Find("skill1").GetComponent<Button>().enabled = false;
    }
    public void Skill2_2()
    {
        if (skillNum < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        skillNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.skillDmg += 12;
        targetTurret.skillExplosionRadius += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill2").GetComponent<Image>().sprite = skillUI;
        GameObject.Find("skill2").GetComponent<Button>().enabled = false;
    }
    public void Skill3_2()
    {
        if (skillNum < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        skillNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.skillDmg += 12;
        targetTurret.skillExplosionRadius += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill3").GetComponent<Image>().sprite = skillUI;
        GameObject.Find("skill3").GetComponent<Button>().enabled = false;
    }
    public void Skill4_2()
    {
        if (skillNum < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        skillNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.skillDmg += 12;
        targetTurret.skillExplosionRadius += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill4").GetComponent<Image>().sprite = skillUI;
        GameObject.Find("skill4").GetComponent<Button>().enabled = false;
    }
    public void Skill5_2()
    {
        if (skillNum < 4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        skillNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.skillDmg += 12;
        targetTurret.skillExplosionRadius += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill5").GetComponent<Image>().sprite = skillUI;
        GameObject.Find("skill5").GetComponent<Button>().enabled = false;
    }
    public void Normal2_2()
    {
        if (nodeLevel < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 5;
        PlayerStats.Money -= 20;
        GameObject.Find("normal2").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal2").GetComponent<Button>().enabled = false;
    }
    public void Normal3_2()
    {
        if (nodeLevel < 3 && !beforeNormal3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 5;
        PlayerStats.Money -= 20;
        GameObject.Find("normal3").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal3").GetComponent<Button>().enabled = false;
    }
    public void Normal4_2()
    {
        if (nodeLevel < 4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 5;
        PlayerStats.Money -= 20;
        GameObject.Find("normal4").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal4").GetComponent<Button>().enabled = false;
    }
    public void Normal5_2()
    {
        if (nodeLevel < 5 && skillNum < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.turretDmg += 5;
        PlayerStats.Money -= 20;
        GameObject.Find("normal5").GetComponent<Image>().sprite = adasUI;
        GameObject.Find("normal5").GetComponent<Button>().enabled = false;
    }
    public void SorcererUpgrade()
    {
        if (nodeLevel < 6 || upgraded)
        {
            return;
        }
        if (PlayerStats.Money < 40)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.sorcererUP = true;
        upgraded = true;
        PlayerStats.Money -= 40;
        GameObject.Find("sorcerer").GetComponent<Image>().sprite = sorcererUI;
        GameObject.Find("sorcerer").GetComponent<Button>().enabled = false;
    }
    public void SummonerUpgrade()
    {
        if (nodeLevel < 6 || upgraded)
        {
            return;
        }
        if (PlayerStats.Money < 40)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.summonerUP = true;
        upgraded = true;
        PlayerStats.Money -= 40;
        GameObject.Find("summoner").GetComponent<Image>().sprite = summonerUI;
        GameObject.Find("summoner").GetComponent<Button>().enabled = false;
    }
    #endregion

    #region Supporter
    public void Normal0_3()
    {
        if (nodeLevel < 0)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal0").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal0").GetComponent<Button>().enabled = false;
    }
    public void Normal1_3()
    {
        if (nodeLevel < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal1").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal1").GetComponent<Button>().enabled = false;
    }
    public void Normal2_3()
    {
        if (nodeLevel < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        nodeLevel++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal2").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal2").GetComponent<Button>().enabled = false;
    }
    public void Buffdmg1_3()
    {
        if (nodeLevel < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        up1Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.dmgBuffScale += 0.06f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffdmg1").GetComponent<Image>().sprite = buffdmgUI;
        GameObject.Find("buffdmg1").GetComponent<Button>().enabled = false;
    }
    public void Buffdmg2_3()
    {
        if (up1Num_3 < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        up1Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.dmgBuffScale += 0.06f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffdmg2").GetComponent<Image>().sprite = buffdmgUI;
        GameObject.Find("buffdmg2").GetComponent<Button>().enabled = false;
    }
    public void Buffdmg3_3()
    {
        if (up1Num_3 < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        up1Num_3++;
        beforeNormal3_3 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.dmgBuffScale += 0.06f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffdmg3").GetComponent<Image>().sprite = buffdmgUI;
        GameObject.Find("buffdmg3").GetComponent<Button>().enabled = false;
    }
    public void Buffas1_3()
    {
        if (nodeLevel < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        down1Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.asBuffScale += 0.05f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffas1").GetComponent<Image>().sprite = buffasUI;
        GameObject.Find("buffas1").GetComponent<Button>().enabled = false;
    }
    public void Buffas2_3()
    {
        if (down1Num_3 < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        down1Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.asBuffScale += 0.05f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffas2").GetComponent<Image>().sprite = buffasUI;
        GameObject.Find("buffas2").GetComponent<Button>().enabled = false;
    }
    public void Buffas3_3()
    {
        if (down1Num_3 < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        down1Num_3++;
        beforeNormal3_3 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.asBuffScale += 0.05f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffas3").GetComponent<Image>().sprite = buffasUI;
        GameObject.Find("buffas3").GetComponent<Button>().enabled = false;
    }
    public void Skill1_3()
    {
        if (nodeLevel < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        middle1Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill1").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("skill1").GetComponent<Button>().enabled = false;
    }
    public void Skill2_3()
    {
        if (middle1Num_3 < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        middle1Num_3++;
        beforeNormal3_3 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill2").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("skill2").GetComponent<Button>().enabled = false;
    }
    public void Normal3_3()
    {
        if (!beforeNormal3_3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal3").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal3").GetComponent<Button>().enabled = false;
    }
    public void Normal4_3()
    {
        if (centerNum < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal4").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal4").GetComponent<Button>().enabled = false;
    }
    public void Normal8_3()
    {
        if (centerNum < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerupNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal8").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal8").GetComponent<Button>().enabled = false;
    }
    public void Normal9_3()
    {
        if (centerupNum < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerupNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal9").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal9").GetComponent<Button>().enabled = false;
    }
    public void Range3_3()
    {
        if (centerupNum < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerupNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1.5f;
        PlayerStats.Money -= 20;
        GameObject.Find("range3").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range3").GetComponent<Button>().enabled = false;
    }
    public void Normal10_3()
    {
        if (centerNum < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerdownNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal10").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal10").GetComponent<Button>().enabled = false;
    }
    public void Skill3_3()
    {
        if (centerdownNum < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerdownNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill3").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("skill3").GetComponent<Button>().enabled = false;
    }
    public void Skill4_3()
    {
        if (centerdownNum < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerdownNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill4").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("skill4").GetComponent<Button>().enabled = false;
    }
    public void Skill5_3()
    {
        if (centerdownNum < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerdownNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.mpGainAmount += 1;
        PlayerStats.Money -= 20;
        GameObject.Find("skill5").GetComponent<Image>().sprite = mpgainUI;
        GameObject.Find("skill5").GetComponent<Button>().enabled = false;
    }
    public void Normal5_3()
    {
        if (centerNum < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal5").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal5").GetComponent<Button>().enabled = false;
    }
    public void Buffdmg4_3()
    {
        if (centerNum < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        up2Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.dmgBuffScale += 0.06f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffdmg4").GetComponent<Image>().sprite = buffdmgUI;
        GameObject.Find("buffdmg4").GetComponent<Button>().enabled = false;
    }
    public void Buffdmg5_3()
    {
        if (up2Num_3 < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        up2Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.dmgBuffScale += 0.06f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffdmg5").GetComponent<Image>().sprite = buffdmgUI;
        GameObject.Find("buffdmg5").GetComponent<Button>().enabled = false;
    }
    public void Buffas4_3()
    {
        if (up2Num_3 < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        up2Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.asBuffScale += 0.05f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffas4").GetComponent<Image>().sprite = buffasUI;
        GameObject.Find("buffas4").GetComponent<Button>().enabled = false;
    }
    public void Buffas5_3()
    {
        if (up2Num_3 < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        up2Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.asBuffScale += 0.05f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffas5").GetComponent<Image>().sprite = buffasUI;
        GameObject.Find("buffas5").GetComponent<Button>().enabled = false;
    }
    public void Buffas6_3()
    {
        if (up2Num_3 < 4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        up2Num_3++;
        beforeNormal6_3 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.asBuffScale += 0.05f;
        PlayerStats.Money -= 20;
        GameObject.Find("buffas6").GetComponent<Image>().sprite = buffasUI;
        GameObject.Find("buffas6").GetComponent<Button>().enabled = false;
    }
    public void Range1_3()
    {
        if (centerNum < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        down2Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1.5f;
        PlayerStats.Money -= 20;
        GameObject.Find("range1").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range1").GetComponent<Button>().enabled = false;
    }
    public void Range2_3()
    {
        if (down2Num_3 < 1)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        down2Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.range += 1.5f;
        PlayerStats.Money -= 20;
        GameObject.Find("range2").GetComponent<Image>().sprite = rangeUI;
        GameObject.Find("range2").GetComponent<Button>().enabled = false;
    }
    public void Slowamount1_3()
    {
        if (down2Num_3 < 2)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        down2Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.slowAmount += 0.07f;
        PlayerStats.Money -= 20;
        GameObject.Find("slowamount1").GetComponent<Image>().sprite = slowamountUI;
        GameObject.Find("slowamount1").GetComponent<Button>().enabled = false;
    }
    public void Slowamount2_3()
    {
        if (down2Num_3 < 3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        down2Num_3++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.slowAmount += 0.07f;
        PlayerStats.Money -= 20;
        GameObject.Find("slowamount2").GetComponent<Image>().sprite = slowamountUI;
        GameObject.Find("slowamount2").GetComponent<Button>().enabled = false;
    }
    public void Slowamount3_3()
    {
        if (down2Num_3 < 4)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        down2Num_3++;
        beforeNormal6_3 = true;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.slowAmount += 0.07f;
        PlayerStats.Money -= 20;
        GameObject.Find("slowamount3").GetComponent<Image>().sprite = slowamountUI;
        GameObject.Find("slowamount3").GetComponent<Button>().enabled = false;
    }
    public void Normal6_3()
    {
        if (!beforeNormal6_3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal6").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal6").GetComponent<Button>().enabled = false;
    }
    public void Normal7_3()
    {
        if (centerNum < 4 && !beforeNormal6_3)
        {
            return;
        }
        if (PlayerStats.Money < 20)
        {
            return;
        }
        centerNum++;
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.damageOverTime += 2;
        PlayerStats.Money -= 20;
        GameObject.Find("normal7").GetComponent<Image>().sprite = dmgUI;
        GameObject.Find("normal7").GetComponent<Button>().enabled = false;
    }
    public void BufferUpgrade()
    {
        if (centerNum < 5 || upgraded)
        {
            return;
        }
        if (PlayerStats.Money < 40)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.bufferUP = true;
        upgraded = true;
        PlayerStats.Money -= 40;
        GameObject.Find("buffer").GetComponent<Image>().sprite = bufferUI;
        GameObject.Find("buffer").GetComponent<Button>().enabled = false;
    }
    public void DebufferUpgrade()
    {
        if (centerNum < 5 || upgraded)
        {
            return;
        }
        if (PlayerStats.Money < 40)
        {
            return;
        }
        targetTurret = turret.GetComponent<Turret>();
        targetTurret.debufferUP = true;
        upgraded = true;
        PlayerStats.Money -= 40;
        GameObject.Find("debuffer").GetComponent<Image>().sprite = debufferUI;
        GameObject.Find("debuffer").GetComponent<Button>().enabled = false;
    }
    #endregion
}
