﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingButton : MonoBehaviour {

    public GameObject image1;

    public string menuSceneName = "MainMenu";
    public SceneFader sceneFader;

    public void Button()
    {
        if (image1.activeSelf)
        {
            sceneFader.FadeTo(menuSceneName);
        }
    }
}
