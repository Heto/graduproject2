﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turret : MonoBehaviour {

    private Transform target;
    public Enemy targetEnemy;
    
    [Header("General")]

    public float range = 15f;
    public int typeOfTurret = 0; // 0 = melee 1 = arrow 2 = magic 3 = support

    [Header("Melee")]
    public Animator anim;
    public EndOfAnim endOfAnim;
    public int numOfStun = 1;
    public float nearEnemyStunRange = 0f;
    public float stunTime = 0f;
    public float knightStunTime = 0f;
    public float berserkerFireRate = 0f;
    public float berserkerModeCountdown = 0f;

    public bool knightUP = false;
    public bool berserkerUP = false;
    public bool berserkerSkill = false;
    public bool berserkerMode = false;

    [Header("Archer / Magician")]
    public GameObject bulletPrefab;
    public GameObject magicPrefab;
    public GameObject supporterSkillPrefab;
    public GameObject poisonPrefab;
    public GameObject executionerBullet;
    public GameObject sorcererSkillPrefab;
    public GameObject summonerTurret;

    [HideInInspector]
    public float fireCountdown = 0f;
    private float normalFireRate = 0f;
    [HideInInspector]
    public float normalTurretDmg = 0f;
    private float buffedFireRate = 0f;
    [HideInInspector]
    public float buffedTurretDmg = 0f;

    public float execuFireRate = 0f;
    public float buffedExecuFireRate = 0f;
    
    public float fireRate = 1f;    
    public float turretDmg = 50;
    public float criticalChance = 0.1f;
    public float criticalDmg = 1.5f;
    public float explosionRadius = 8f;
    public GameObject runan1Obj;
    public GameObject runan2Obj;

    public bool rangerUP = false;
    public bool executionerUP = false;

    [Header("Magician")]
    private float mp = 0f;
    public float mpGainAmount = 20f;
    public Image mpBar;
    public int skillDmg = 0;
    public float skillExplosionRadius = 0f;

    public bool sorcererUP = false;
    public bool summonerUP = false;
    public Turret targetTurret = null;
    public float summonerRange = 30f;
    //private GameObject nearestTurret = null;
    public AudioSource sound;
    public GameObject birdPosition;

    public static bool summonerSkillON = false;

    [Header("Supporter")]
    public int damageOverTime = 30;
    public float slowAmount = 0.5f;
    public int numOfTarget = 1;
    public float dmgBuffScale = 0f;
    public float asBuffScale = 0f;    
    public bool buffed = false;

    public bool alreadybuffed = false;
    public bool debufferUP = false;
    public bool bufferUP = false;
    public float slowTime = 0f;
    public float debufferSlowTime = 0f;
    public static bool debuffON = false;
    public static bool buffON = false;
    public SphereCollider buffcol;

    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
    public Light impactLight;
    public GameObject buffedEffect;

    [Header("Unity Setup Fields")]

    public string enemyTag = "Enemy";

    public Transform partToRotate;
    public float turnSpeed = 10f;
        
    public Transform firePoint;

    public Animator anim_1;
    public Animator anim_1_1;
    public Animator anim_1_2;
    public Animator anim_3;

    public GameObject rangeObject;

    // Use this for initialization
    void Start () {
        sound = GetComponent<AudioSource>();

        InvokeRepeating("UpdateTarget", 0f, 0.5f);
	}

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach(GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        } else
        {
            target = null;
        }
    }
	
	// Update is called once per frame
	void Update () {

        fireCountdown -= Time.deltaTime;
        fireCountdown = Mathf.Clamp(fireCountdown, 0f, Mathf.Infinity);
        
        if (berserkerMode)
        {
            berserkerFireRate -= 0.2f * Time.deltaTime;
            berserkerFireRate = Mathf.Clamp(berserkerFireRate, 0.1f, Mathf.Infinity);

            berserkerModeCountdown -= Time.deltaTime;
            berserkerModeCountdown = Mathf.Clamp(berserkerModeCountdown, 0f, Mathf.Infinity);

            if (berserkerModeCountdown <= 0f)
            {
                berserkerSkill = true;
                berserkerMode = false;
            }
        } else
        {
            normalFireRate = fireRate;
            buffedFireRate = fireRate + fireRate * asBuffScale;
        }

        if (target == null)
        {
            if (typeOfTurret == 3)
            {
                if (lineRenderer.enabled)
                {
                    if (bufferUP)
                        anim_1_1.SetBool("Attack", false);
                    else if (debufferUP)
                        anim_1_2.SetBool("Attack", false);
                    else
                        anim_1.SetBool("Attack", false);

                    lineRenderer.enabled = false;
                    impactEffect.Stop();
                    impactLight.enabled = false;
                }

                if (bufferUP)
                {
                    anim_1_1.gameObject.SetActive(true);
                    anim_1.gameObject.SetActive(false);
                }
                else if (debufferUP)
                {
                    anim_1_2.gameObject.SetActive(true);
                    anim_1.gameObject.SetActive(false);
                }
            }
            else if (typeOfTurret == 0)
            {
                if (knightUP)
                {
                    anim_1_1.gameObject.SetActive(true);
                    anim_1.gameObject.SetActive(false);
                }
                else if (berserkerUP)
                {
                    anim_1_2.gameObject.SetActive(true);
                    anim_1.gameObject.SetActive(false);
                }
            }
            else if (typeOfTurret == 1)
            {
                if (rangerUP)
                {
                    anim_1_1.gameObject.SetActive(true);
                    anim_1.gameObject.SetActive(false);
                }
                else if (executionerUP)
                {
                    anim_1_2.gameObject.SetActive(true);
                    anim_1.gameObject.SetActive(false);
                }
            }
            else if (typeOfTurret == 2)
            {
                if (sorcererUP)
                {
                    anim_1_1.gameObject.SetActive(true);
                    anim_1.gameObject.SetActive(false);
                }
                else if (summonerUP)
                {
                    anim_1_2.gameObject.SetActive(true);
                    anim_1.gameObject.SetActive(false);
                    summonerSkillON = true;
                }
            }

            if (buffed)
                buffedEffect.SetActive(true);
            else
                buffedEffect.SetActive(false);
            return;
        }            

        LockOnTarget();
               
        normalTurretDmg = turretDmg;        
        buffedTurretDmg = turretDmg + turretDmg * dmgBuffScale;
        
        if (typeOfTurret == 3)
        {
            Laser();

            if (bufferUP)
            {
                anim_1_1.gameObject.SetActive(true);
                anim_1.gameObject.SetActive(false);
            }
            else if (debufferUP)
            {
                anim_1_2.gameObject.SetActive(true);
                anim_1.gameObject.SetActive(false);
            }

            if (mp >= 100)
            {
                UseSupporterSkill();
            }

            if (debufferUP)
                debuffON = true;
            if (bufferUP)
            {
                buffON = true;
                buffcol.radius = 110;
            } else
            {
                buffON = false;
                buffcol.radius = 12;
            }
                

        }
        else if (typeOfTurret == 1)
        {
            if (rangerUP)
            {
                anim_1_1.gameObject.SetActive(true);
                anim_1.gameObject.SetActive(false);
            }
            else if (executionerUP)
            {
                anim_1_2.gameObject.SetActive(true);
                anim_1.gameObject.SetActive(false);
            }

            execuFireRate = normalFireRate * 0.5f;
            buffedExecuFireRate = buffedFireRate * 0.5f;

            if (fireCountdown <= 0f)
            {
                if (rangerUP)
                    PoisonArrowShoot();
                else if (executionerUP)
                    ExecutionerBulletShoot();
                else
                    Shoot();

                if (executionerUP)
                {
                    if (buffed)
                        fireCountdown = 1f / buffedExecuFireRate;
                    else
                        fireCountdown = 1f / execuFireRate;
                } else
                {
                    if (buffed)
                        fireCountdown = 1f / buffedFireRate;
                    else
                        fireCountdown = 1f / normalFireRate;
                }
                
            }
            
            
        }
        else if (typeOfTurret == 2)
        {
            if (sorcererUP)
            {
                anim_1_1.gameObject.SetActive(true);
                anim_1.gameObject.SetActive(false);
            }
            else if (summonerUP)
            {
                anim_1_2.gameObject.SetActive(true);
                anim_1.gameObject.SetActive(false);
            }
            if (fireCountdown <= 0f)
            {
                if (mp >= 100)
                {
                    if (!sorcererUP)
                        UseMagicineSkill();
                    else
                        UseSorcererSkill();
                }
                else
                {
                    Shoot();
                }

                if (buffed)
                    fireCountdown = 1f / buffedFireRate;
                else
                    fireCountdown = 1f / normalFireRate;
            }

            if (summonerUP)
            {
                //if (nearestTurret == null)
                summonerSkillON = true;
            }
            
        }
        else if (typeOfTurret == 0)
        {
            if (knightUP)
            {
                anim_1_1.gameObject.SetActive(true);
                anim_1.gameObject.SetActive(false);
            }
            else if (berserkerUP)
            {
                anim_1_2.gameObject.SetActive(true);
                anim_1.gameObject.SetActive(false);
            }
            if (fireCountdown <= 0f)
            {
                Strike();
                Enemy e = target.GetComponent<Enemy>();
                if (e.isStunedEver == false)
                    StunAttack();

                if (!berserkerMode)
                {
                    if (buffed)
                        fireCountdown = 1f / buffedFireRate;
                    else
                        fireCountdown = 1f / normalFireRate;
                } else
                {
                    fireCountdown = 1f / berserkerFireRate;
                }                
            }
            knightStunTime = stunTime * 2;
        }        
        
    }

    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void Laser()
    {
        if (bufferUP)
            anim_1_1.SetBool("Attack", true);
        else if (debufferUP)
            anim_1_2.SetBool("Attack", true);
        else
            anim_1.SetBool("Attack", true);

        targetEnemy.TakeDamage(damageOverTime * Time.deltaTime);
        targetEnemy.Slow(slowAmount);
        if (debufferUP)
            targetEnemy.DebufferAtk();
        GainMp(mpGainAmount * Time.deltaTime);

        if (!lineRenderer.enabled)
        {
            lineRenderer.enabled = true;
            impactEffect.Play();
            impactLight.enabled = true;
        }

        lineRenderer.SetPosition(0, firePoint.position);
        lineRenderer.SetPosition(1, target.position);

        Vector3 dir = firePoint.position - target.position;

        impactEffect.transform.position = target.position + dir.normalized;

        impactEffect.transform.rotation = Quaternion.LookRotation(dir);

        if (debufferUP)
        {
            targetEnemy.laserSplash = true;
        }

    }

    void Shoot()
    {
        if (typeOfTurret == 1 || typeOfTurret == 2)
        {
            if (sorcererUP)
                anim_1_1.SetTrigger("Attack");
            else if (summonerUP)
                anim_1_2.SetTrigger("Attack");
            else
                anim_1.SetTrigger("Attack");
        }
            
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        if (buffed)
            bullet.damage = buffedTurretDmg;
        else
            bullet.damage = normalTurretDmg;
        bullet.criticalChance = criticalChance;
        bullet.criticalDmg = criticalDmg;
        bullet.explosionRadius = explosionRadius;
        bullet.mpGainAmount = mpGainAmount;
        bullet.turret = this.GetComponent<Turret>();

        if (bullet != null)
            bullet.Seek(target);
    }

    void UseMagicineSkill()
    {
        anim_1.SetTrigger("Skill");
        //sound.Play();
        mp = 0;
        GameObject bulletGO = (GameObject)Instantiate(magicPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        bullet.turret = this.GetComponent<Turret>();
        mpBar.fillAmount = mp / 100;
        bullet.damage = skillDmg;
        bullet.explosionRadius = skillExplosionRadius;

        if (bullet != null)
            bullet.Seek(target);
    }

    void UseSorcererSkill()
    {
        anim_1_1.SetTrigger("Skill");
        StartCoroutine(SorcererSoundPlay());
        mp = 0;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);        
        foreach (GameObject enemy in enemies)
        {
            GameObject skillGO = (GameObject)Instantiate(sorcererSkillPrefab, new Vector3(enemy.transform.position.x, enemy.transform.position.y + 65, enemy.transform.position.z), Quaternion.Euler(90, 0, 0));
            skillGO.transform.SetParent(enemy.transform);
            SorcererSkill sk = skillGO.GetComponent<SorcererSkill>();
            sk.turret = this.GetComponent<Turret>();
            sk.target = enemy.GetComponent<Enemy>();
        }
        mpBar.fillAmount = mp / 100;
    }
    IEnumerator SorcererSoundPlay()
    {
        yield return new WaitForSeconds(0.5f);
        sound.Play();
    }

    void UseSupporterSkill()
    {
        mp = 0;
        sound.Play();
        GameObject bulletGO = (GameObject)Instantiate(supporterSkillPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        bullet.turret = this.GetComponent<Turret>();
        mpBar.fillAmount = mp / 100;
        bullet.explosionRadius = skillExplosionRadius;
        if (debufferUP)
        {
            bullet.slowAmount = debufferSlowTime;
            bullet.damage = skillDmg;
        }            
        else
        {
            bullet.slowAmount = slowTime;
            bullet.damage = 1;
        }
            

        if (bullet != null)
            bullet.Seek(target);
    }

    void Strike()
    {
        if (knightUP)
            anim_1_1.SetTrigger("Attack");
        else if (berserkerUP)
            anim_1_2.SetTrigger("Attack");
        else
            anim.SetTrigger("Attack");
    }

    void StunAttack()
    {
        Enemy e = target.GetComponent<Enemy>();
        if (knightUP)
            e.Stun(knightStunTime);
        else
            e.Stun(stunTime);
        StunNearEnemies(e);
    }

    void StunNearEnemies(Enemy enemy)
    {
        Collider[] colliders = Physics.OverlapSphere(enemy.transform.position, nearEnemyStunRange);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                Enemy e = collider.GetComponent<Enemy>();
                if (knightUP)
                    e.Stun(knightStunTime);
                else
                    e.Stun(stunTime);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    public void RunanActive(int num)
    {
        if (num == 1)
            runan1Obj.SetActive(true);
        if (num == 2)
            runan2Obj.SetActive(true);
    }

    public void GainMp(float amount)
    {
        mp += amount;

        mpBar.fillAmount = mp / 100;
    }

    void OnTriggerEnter(Collider other)
    {
        if (typeOfTurret == 3)
        {
            if (other.tag == "Turret")
            {
                Turret t = other.GetComponent<Turret>();
                t.dmgBuffScale = dmgBuffScale;
                t.asBuffScale = asBuffScale;                
                t.GetBuff();                
            }
        }        
    }
    void OnTriggerStay(Collider other)
    {
        if (buffON)
            BufferEffect();
    }
    void OnTriggerExit(Collider other)
    {
        if (typeOfTurret == 3)
        {
            if (other.tag == "Turret")
            {
                Turret t = other.GetComponent<Turret>();
                t.LoseBuff();
            }
        }            
    }

    public void GetBuff()
    {
        buffed = true;
    }

    public void LoseBuff()
    {
        buffed = false;
    }

    public void BerserkerSkillUse()
    {
        berserkerSkill = false;
        berserkerMode = true;
        berserkerModeCountdown = 5f;
        if (buffed)
            berserkerFireRate = buffedFireRate + buffedFireRate;
        else
            berserkerFireRate = normalFireRate + normalFireRate;
    }

    void PoisonArrowShoot()
    {
        anim_1_1.SetTrigger("Attack");
        GameObject bulletGO = (GameObject)Instantiate(poisonPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        if (buffed)
            bullet.damage = buffedTurretDmg;
        else
            bullet.damage = normalTurretDmg;
        bullet.criticalChance = criticalChance;
        bullet.criticalDmg = criticalDmg;
        bullet.turret = this.GetComponent<Turret>();

        if (bullet != null)
            bullet.Seek(target);
    }

    void ExecutionerBulletShoot()
    {
        anim_1_2.SetTrigger("Attack");
        GameObject bulletGO = (GameObject)Instantiate(executionerBullet, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        if (buffed)
            bullet.damage = buffedTurretDmg;
        else
            bullet.damage = normalTurretDmg;
        bullet.criticalChance = criticalChance;
        bullet.criticalDmg = criticalDmg;
        bullet.turret = this.GetComponent<Turret>();

        if (bullet != null)
            bullet.Seek(target);
    }

    void SummonerSkill()
    {

        //GameObject[] turrets = GameObject.FindGameObjectsWithTag("Turret");
        //float sum_shortestDistance = Mathf.Infinity;        
        //foreach (GameObject turret in turrets)
        //{
        //    if (turret.Equals(this.gameObject))
        //        continue;
        //    float sum_distanceToTurret = Vector3.Distance(transform.position, turret.transform.position);
        //    if (sum_distanceToTurret < sum_shortestDistance)
        //    {
        //        sum_shortestDistance = sum_distanceToTurret;
        //        nearestTurret = turret;
        //    }
        //}
        //if (nearestTurret != null)
        //{
        //    targetTurret = nearestTurret.GetComponent<Turret>();
        //}
        //else
        //{
        //    targetTurret = null;
        //}
        
        //GameObject _turret = (GameObject)Instantiate(summonerTurret, birdPosition.transform.position, Quaternion.identity);
        //Turret summonedTurret = _turret.GetComponent<Turret>();
        //summonedTurret.turretDmg = targetTurret.turretDmg;
        //summonedTurret.fireRate = targetTurret.fireRate;
        //summonedTurret.range = targetTurret.range;
        //summonedTurret.criticalChance = targetTurret.criticalChance;
        //summonedTurret.criticalDmg = targetTurret.criticalDmg;
        //summonedTurret.explosionRadius = targetTurret.explosionRadius;
    }

    void BufferEffect()
    {
        if (!alreadybuffed)
        {
            alreadybuffed = true;
            if (typeOfTurret == 0)
            {
                stunTime += 1f;
            }
            else if (typeOfTurret == 1)
            {
                criticalDmg += 0.2f;
            }
            else if (typeOfTurret == 2)
            {
                mpGainAmount += 2f;
            }
        }
        
    }

}
