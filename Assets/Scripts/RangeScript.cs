﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeScript : MonoBehaviour {

    public Turret turret;
	
	// Update is called once per frame
	void Update () {
        transform.localScale = new Vector3(turret.range * 2, turret.range * 2, turret.range * 2);
	}
}
