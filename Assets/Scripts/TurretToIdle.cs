﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretToIdle : MonoBehaviour {

    public AudioSource sound;

    void Start()
    {
        sound = GetComponent<AudioSource>();
    }

    public void SoundPlay()
    {
        sound.Play();
    }

    public void ToIdle()
    {
        Animator anim = GetComponent<Animator>();
        anim.SetTrigger("ToIdle");
    }
}
