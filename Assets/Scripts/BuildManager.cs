﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour {

    public static BuildManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one buildManager in scene!");
            return;
        }
        instance = this;
    }
    
    public GameObject buildEffect;
    public GameObject sellEffect;

    private TurretBlueprint turretToBuild;
    private Node selectedNode;

    public NodeUI nodeUI;

    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.Money >= turretToBuild.cost; } }

    public void SelectNode(Node node)
    {
        if (selectedNode == node)
        {
            DeselectNode();
            return;
        }

        selectedNode = node;
        turretToBuild = null;

        nodeUI = node.targetTurret.GetComponentInChildren<NodeUI>();
        nodeUI.SetTarget(node);
    }

    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            if (TutorialManager.tutorialProgress == 6)
                return;

            if (GameObject.FindObjectOfType<NodeUI>())
            {
                NodeUI nodes = GameObject.FindObjectOfType<NodeUI>();
                nodes.ExitButton();
            }

            DeselectNode();
            turretToBuild = null;
        }
    }
    public void DeselectNode()
    {
        if (nodeUI == null)
            return;
        selectedNode = null;
        nodeUI.Hide();
    }
       
    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        turretToBuild = turret;
        DeselectNode();

        if (TutorialManager.tutorialProgress == 3)
        {
            TutorialManager.tutorialProgress = 4;
        }
    }

    public TurretBlueprint GetTurretToBuild()
    {
        return turretToBuild;
    }
}
