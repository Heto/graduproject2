﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroButton : MonoBehaviour {

    public GameObject image1;
    public GameObject image2;
    public GameObject image3;

    public string menuSceneName = "MainLevel";
    public SceneFader sceneFader;

    public void Button()
    {
        if (image1.activeSelf)
        {
            image1.SetActive(false);
            image2.SetActive(true);
        }
        else if (image2.activeSelf)
        {
            image2.SetActive(false);
            image3.SetActive(true);
        }
        else if (image3.activeSelf)
        {
            sceneFader.FadeTo(menuSceneName);
        }
    }
}
