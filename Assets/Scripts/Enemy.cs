﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public float startSpeed = 10f;

    [HideInInspector]
    public float speed;

    public float starthealth = 100;
    private float health;

    public int worth = 50;

    public float armor = 0f;

    public GameObject deathEffect;

    [Header("Unity Stuff")]
    public Image healthBar;
        
    public bool isDead = false;
    public string enemyTag = "Enemy";
    public float range = 8f;
    public int damageOverTime = 30;
    public float slowAmount = 0.5f;

    public bool laserSplash = false;

    public bool isStun = false;
    public bool isStunedEver = false;
    public bool isSlow = false;
    private float stunCountdown = 0f;
    private float slowCountdown = 0f;

    public int poisonStack = 0;
    public bool isPoisoning = false;
    private float poisonCountdown = 0f;
    private float poisonDamageCountdown = 0f;

    private Transform target;
    public Enemy targetEnemy;
    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
    public Light impactLight;
    public Transform firePoint;
    public Animator anim;
    public GameObject moneyCanvas;
    public Text moneyText;
    public GameObject stunEffect;
    public GameObject slowEffect;
    public GameObject summonerEffect;

    void Start()
    {
        speed = startSpeed;
        health = starthealth;

        if (Turret.debuffON)
        {
            if (armor > 0)
                armor = armor / 2;
        }

        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            if (enemy.Equals(this.gameObject))
                continue;
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            target = null;
        }
    }

    void Update()
    {
        if (target == null)
        {
            if (lineRenderer.enabled)
            {
                lineRenderer.enabled = false;
                impactEffect.Stop();
                impactLight.enabled = false;
                laserSplash = false;
            }

            return;
        }

        if (laserSplash)
        {
            Laser();
        }

        if (stunCountdown <= 0f)
        {
            isStun = false;
        }
        stunCountdown -= Time.deltaTime;
        stunCountdown = Mathf.Clamp(stunCountdown, 0f, Mathf.Infinity);

        if (slowCountdown <= 0f)
        {
            isSlow = false;
        }
        slowCountdown -= Time.deltaTime;
        slowCountdown = Mathf.Clamp(slowCountdown, 0f, Mathf.Infinity);

        if (isStun)
        {
            speed = 0f;
            stunEffect.SetActive(true);
        }            
        else if (isSlow)
        {
            speed = startSpeed * (1f - 0.9f);
            //slowEffect.SetActive(true);
        }            
        else
        {
            speed = startSpeed;
            stunEffect.SetActive(false);
            //slowEffect.SetActive(false);
        }            

        if (isPoisoning)
        {
            poisonCountdown -= Time.deltaTime;
            poisonCountdown = Mathf.Clamp(poisonCountdown, 0f, Mathf.Infinity);

            poisonDamageCountdown -= Time.deltaTime;
            poisonDamageCountdown = Mathf.Clamp(poisonDamageCountdown, 0f, Mathf.Infinity);

            if (poisonDamageCountdown <= 0)
            {
                PoisonDamage();
                poisonDamageCountdown = 1f;
            }

            if (poisonCountdown <= 0)
            {
                isPoisoning = false;
            }                
        }
        
    }

    void LateUpdate()
    {
        laserSplash = false;
    }

    void Laser()
    {
        targetEnemy.TakeDamage(damageOverTime * Time.deltaTime);
        targetEnemy.Slow(slowAmount);

        if (!lineRenderer.enabled)
        {
            lineRenderer.enabled = true;
            impactEffect.Play();
            impactLight.enabled = true;
        }

        lineRenderer.SetPosition(0, firePoint.position);
        lineRenderer.SetPosition(1, target.position);

        Vector3 dir = firePoint.position - target.position;

        impactEffect.transform.position = target.position + dir.normalized;

        //impactEffect.transform.rotation = Quaternion.LookRotation(dir);

    }

    public void TakeDamage(float amount)
    {
        if (armor < amount && armor > 0)
            health -= (amount - armor);
        else if (armor > amount)
            health -= 0.5f;
        else
            health -= amount;

        healthBar.fillAmount = health / starthealth;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public void TakeTrueDamage(float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / starthealth;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public void TakeLegoDamage(float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / starthealth;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public void Slow (float pct)
    {
        speed = startSpeed * (1f - pct);
    }    

    public void DebufferAtk()
    {
        laserSplash = true;
    }

    void Die()
    {
        isDead = true;

        moneyText.text = "+" + worth;
        moneyCanvas.SetActive(true);
        moneyCanvas.transform.Translate(Vector3.up * 30f * Time.deltaTime, Space.World);

        PlayerStats.Money += worth;

        WaveSpawner.EnemiesAlive--;
        GameManager.enemyKillStrick++;

        gameObject.tag = "Untagged";

        anim.SetTrigger("Dead");
    }

    public void DestroyObject()
    {
        Destroy(gameObject);
    }

    public void Stun(float Amount)
    {
        isStun = true;
        stunCountdown = Amount;
        isStunedEver = true;
    }

    public void SlowSkill(float slowTime)
    {
        isSlow = true;
        slowCountdown = slowTime;
    }

    public void Poison()
    {
        isPoisoning = true;
        poisonCountdown = 5f;
        poisonDamageCountdown = 1f;
    }
    public void PoisonDamage()
    {
        health -= starthealth * 0.05f * poisonStack;
        healthBar.fillAmount = health / starthealth;
        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    void OnMouseOver()
    {
        if (Turret.summonerSkillON && Input.GetMouseButtonDown(0))
        {
            if (health > starthealth / 2)
            {
                Instantiate(summonerEffect, transform.position, Quaternion.identity);
                health -= 10f;
                healthBar.fillAmount = health / starthealth;
                if (health <= 0 && !isDead)
                {
                    Die();
                }
            }            
        }
    }
}
