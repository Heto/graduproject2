﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeMouseOver : MonoBehaviour {

    public Text text1;
    public Text text2;
    public Text price;
    public NodeUI targetNodeUI;

    public GameObject panel;

    public int typeOfTurret;

    public void MouseOver()
    {
        panel.SetActive(true);
        panel.transform.position = Input.mousePosition;

        #region Melee
        if (typeOfTurret == 0)
        {
            if (this.name == "normal0")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal1")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal9")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal10")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "stunnum1")
            {
                targetNodeUI.target.stunRange = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "스턴범위 +" + targetNodeUI.target.stunRange;
                text2.text = null;
            }
            if (this.name == "normal2")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "range1")
            {
                targetNodeUI.target.range = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "stuntime1")
            {
                targetNodeUI.target.stunTime = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "스턴시간 +" + targetNodeUI.target.stunTime;
                text2.text = null;
            }
            if (this.name == "stuntime2")
            {
                targetNodeUI.target.stunTime = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "스턴시간 +" + targetNodeUI.target.stunTime;
                text2.text = null;
            }
            if (this.name == "as1")
            {
                targetNodeUI.target.aspd = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "as2")
            {
                targetNodeUI.target.aspd = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "as3")
            {
                targetNodeUI.target.aspd = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "normal3")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal4")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal5")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "range2")
            {
                targetNodeUI.target.range = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "stuntime3")
            {
                targetNodeUI.target.stunTime = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "스턴시간 +" + targetNodeUI.target.stunTime;
                text2.text = null;
            }
            if (this.name == "stuntime4")
            {
                targetNodeUI.target.stunTime = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "스턴시간 +" + targetNodeUI.target.stunTime;
                text2.text = null;
            }
            if (this.name == "stuntimeSqu")
            {
                targetNodeUI.target.stunTime = 0.4f;
                targetNodeUI.target.cost = 20;
                text1.text = "스턴시간 +" + targetNodeUI.target.stunTime;
                text2.text = null;
            }
            if (this.name == "stunnum2")
            {
                targetNodeUI.target.stunRange = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "스턴범위 +" + targetNodeUI.target.stunRange;
                text2.text = null;
            }
            if (this.name == "as4")
            {
                targetNodeUI.target.aspd = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "as5")
            {
                targetNodeUI.target.aspd = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "dmg1")
            {
                targetNodeUI.target.dmg = 3f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "dmg2")
            {
                targetNodeUI.target.dmg = 3f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "dmgSqu")
            {
                targetNodeUI.target.dmg = 6f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "normal6")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal7")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal11")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal12")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal13")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "stunnum3")
            {
                targetNodeUI.target.stunRange = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "스턴범위 +" + targetNodeUI.target.stunRange;
                text2.text = null;
            }
            if (this.name == "normal8")
            {
                targetNodeUI.target.dmg = 1.5f;
                targetNodeUI.target.aspd = 0.035f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "knight")
            {
                targetNodeUI.target.cost = 40;
                //text1.text = "나이트로 전직";
                //text2.text = null;
            }
            if (this.name == "berserker")
            {
                targetNodeUI.target.cost = 40;
                //text1.text = "버서커로 전직";
                //text2.text = null;
            }
        }
        #endregion
        
        #region Archer
        if (typeOfTurret == 1)
        {
            if (this.name == "normal0")
            {
                targetNodeUI.target.dmg = 2;
                targetNodeUI.target.aspd = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal1")
            {
                targetNodeUI.target.dmg = 2;
                targetNodeUI.target.aspd = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal2")
            {
                targetNodeUI.target.dmg = 2;
                targetNodeUI.target.aspd = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal3")
            {
                targetNodeUI.target.dmg = 2;
                targetNodeUI.target.aspd = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "range1")
            {
                targetNodeUI.target.range = 1.5f;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "range2")
            {
                targetNodeUI.target.range = 1.5f;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "range3")
            {
                targetNodeUI.target.range = 2f;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;                              
            }
            if (this.name == "normal4")
            {
                targetNodeUI.target.dmg = 2;
                targetNodeUI.target.aspd = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "crichan1")
            {
                targetNodeUI.target.crichan = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "치명확률 +" + "10%";
                text2.text = null;
            }
            if (this.name == "crichan2")
            {
                targetNodeUI.target.crichan = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "치명확률 +" + "10%";
                text2.text = null;
            }
            if (this.name == "crichan3")
            {
                targetNodeUI.target.crichan = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "치명확률 +" + "10%";
                text2.text = null;
            }
            if (this.name == "cridmg1")
            {
                targetNodeUI.target.cridmg = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "치명피해 +" + targetNodeUI.target.cridmg;
                text2.text = null;
            }
            if (this.name == "cridmg2")
            {
                targetNodeUI.target.cridmg = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "치명피해 +" + targetNodeUI.target.cridmg;
                text2.text = null;
            }
            if (this.name == "criSqu")
            {
                targetNodeUI.target.cridmg = 0.2f;
                targetNodeUI.target.crichan = 0.1f;
                targetNodeUI.target.cost = 20;
                text1.text = "치명확률 +" + "10%";
                text2.text = "치명피해 +" + targetNodeUI.target.cridmg;
            }
            if (this.name == "normal5")
            {
                targetNodeUI.target.dmg = 2;
                targetNodeUI.target.aspd = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "as1")
            {
                targetNodeUI.target.aspd = 0.2f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "as2")
            {
                targetNodeUI.target.aspd = 0.2f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "as3")
            {
                targetNodeUI.target.aspd = 0.2f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "asSqu")
            {
                targetNodeUI.target.aspd = 0.3f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "dmg1")
            {
                targetNodeUI.target.dmg = 3;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "dmg2")
            {
                targetNodeUI.target.dmg = 3;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "dmg3")
            {
                targetNodeUI.target.dmg = 3;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "dmgSqu")
            {
                targetNodeUI.target.dmg = 10;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "normal6")
            {
                targetNodeUI.target.dmg = 2;
                targetNodeUI.target.aspd = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "normal7")
            {
                targetNodeUI.target.dmg = 4;
                targetNodeUI.target.aspd = 0.08f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "runan1")
            {
                targetNodeUI.target.cost = 20;
                text1.text = "보조 화살 +1";
                text2.text = null;
            }
            if (this.name == "runan2")
            {
                targetNodeUI.target.cost = 20;
                text1.text = "보조 화살 +1";
                text2.text = null;
            }
            if (this.name == "ranger")
            {
                targetNodeUI.target.cost = 40;
                //text1.text = "레인저로 전직";
                //text2.text = null;
            }
            if (this.name == "executioner")
            {
                targetNodeUI.target.cost = 40;
                //text1.text = "처형인으로 전직";
                //text2.text = null;
            }
        }
        #endregion

        #region Magician
        if (typeOfTurret == 2)
        {
            if (this.name == "normal0")
            {
                targetNodeUI.target.dmg = 5;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "normal1")
            {
                targetNodeUI.target.dmg = 5;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "radius1")
            {
                targetNodeUI.target.radius = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "폭발범위 +" + targetNodeUI.target.radius;
                text2.text = null;
            }
            if (this.name == "radius2")
            {
                targetNodeUI.target.radius = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "폭발범위 +" + targetNodeUI.target.radius;
                text2.text = null;
            }
            if (this.name == "radius3")
            {
                targetNodeUI.target.radius = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "폭발범위 +" + targetNodeUI.target.radius;
                text2.text = null;
            }
            if (this.name == "radius4")
            {
                targetNodeUI.target.radius = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "폭발범위 +" + targetNodeUI.target.radius;
                text2.text = null;
            }
            if (this.name == "mpgain1")
            {
                targetNodeUI.target.mpGain = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "mpgain2")
            {
                targetNodeUI.target.mpGain = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "mpgain3")
            {
                targetNodeUI.target.mpGain = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "mpgain4")
            {
                targetNodeUI.target.mpGain = 1f;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "as1")
            {
                targetNodeUI.target.aspd = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "as2")
            {
                targetNodeUI.target.aspd = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "as3")
            {
                targetNodeUI.target.aspd = 0.15f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격속도 +" + targetNodeUI.target.aspd;
                text2.text = null;
            }
            if (this.name == "allSqu")
            {
                targetNodeUI.target.radius = 1f;
                targetNodeUI.target.mpGain = 1f;
                targetNodeUI.target.aspd = 0.2f;
                targetNodeUI.target.cost = 20;
                text1.text = "폭발범위,마나수급++";
                text2.text = "공격속도 +" + targetNodeUI.target.aspd;
            }
            if (this.name == "range1")
            {
                targetNodeUI.target.range = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "range2")
            {
                targetNodeUI.target.range = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "range3")
            {
                targetNodeUI.target.range = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "range4")
            {
                targetNodeUI.target.range = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "skill1")
            {
                targetNodeUI.target.skillDmg = 12;
                targetNodeUI.target.skillRadius = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "스킬피해 +" + targetNodeUI.target.skillDmg;
                text2.text = "스킬범위 +" + targetNodeUI.target.skillRadius;
            }
            if (this.name == "skill2")
            {
                targetNodeUI.target.skillDmg = 12;
                targetNodeUI.target.skillRadius = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "스킬피해 +" + targetNodeUI.target.skillDmg;
                text2.text = "스킬범위 +" + targetNodeUI.target.skillRadius;
            }
            if (this.name == "skill3")
            {
                targetNodeUI.target.skillDmg = 12;
                targetNodeUI.target.skillRadius = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "스킬피해 +" + targetNodeUI.target.skillDmg;
                text2.text = "스킬범위 +" + targetNodeUI.target.skillRadius;
            }
            if (this.name == "skill4")
            {
                targetNodeUI.target.skillDmg = 12;
                targetNodeUI.target.skillRadius = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "스킬피해 +" + targetNodeUI.target.skillDmg;
                text2.text = "스킬범위 +" + targetNodeUI.target.skillRadius;
            }
            if (this.name == "skill5")
            {
                targetNodeUI.target.skillDmg = 12;
                targetNodeUI.target.skillRadius = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "스킬피해 +" + targetNodeUI.target.skillDmg;
                text2.text = "스킬범위 +" + targetNodeUI.target.skillRadius;
            }
            if (this.name == "normal2")
            {
                targetNodeUI.target.dmg = 5;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "normal3")
            {
                targetNodeUI.target.dmg = 5;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "normal4")
            {
                targetNodeUI.target.dmg = 5;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "normal5")
            {
                targetNodeUI.target.dmg = 5;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.dmg;
                text2.text = null;
            }
            if (this.name == "sorcerer")
            {
                targetNodeUI.target.cost = 40;
                //text1.text = "소서러로 전직";
                //text2.text = null;
            }
            if (this.name == "summoner")
            {
                targetNodeUI.target.cost = 40;
                //text1.text = "서머너로 전직";
                //text2.text = null;
            }
        }
        #endregion

        #region Supporter
        if (typeOfTurret == 3)
        {
            if (this.name == "normal0")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "normal1")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "normal2")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "buffdmg1")
            {
                targetNodeUI.target.dmgBuff = 0.06f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력버프 +" + targetNodeUI.target.dmgBuff;
                text2.text = null;
            }
            if (this.name == "buffdmg2")
            {
                targetNodeUI.target.dmgBuff = 0.06f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력버프 +" + targetNodeUI.target.dmgBuff;
                text2.text = null;
            }
            if (this.name == "buffdmg3")
            {
                targetNodeUI.target.dmgBuff = 0.06f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력버프 +" + targetNodeUI.target.dmgBuff;
                text2.text = null;
            }
            if (this.name == "buffas1")
            {
                targetNodeUI.target.asBuff = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공속버프 +" + targetNodeUI.target.asBuff;
                text2.text = null;
            }
            if (this.name == "buffas2")
            {
                targetNodeUI.target.asBuff = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공속버프 +" + targetNodeUI.target.asBuff;
                text2.text = null;
            }
            if (this.name == "buffas3")
            {
                targetNodeUI.target.asBuff = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공속버프 +" + targetNodeUI.target.asBuff;
                text2.text = null;
            }
            if (this.name == "skill1")
            {
                targetNodeUI.target.mpGain = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "skill2")
            {
                targetNodeUI.target.mpGain = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "normal3")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "normal4")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "normal8")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "normal9")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "range3")
            {
                targetNodeUI.target.range = 1.5f;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "normal10")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "skill3")
            {
                targetNodeUI.target.mpGain = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "skill4")
            {
                targetNodeUI.target.mpGain = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "skill5")
            {
                targetNodeUI.target.mpGain = 1;
                targetNodeUI.target.cost = 20;
                text1.text = "마나수급 +" + targetNodeUI.target.mpGain;
                text2.text = null;
            }
            if (this.name == "normal5")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "buffdmg4")
            {
                targetNodeUI.target.dmgBuff = 0.06f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력버프 +" + targetNodeUI.target.dmgBuff;
                text2.text = null;
            }
            if (this.name == "buffdmg5")
            {
                targetNodeUI.target.dmgBuff = 0.06f;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력버프 +" + targetNodeUI.target.dmgBuff;
                text2.text = null;
            }
            if (this.name == "buffas4")
            {
                targetNodeUI.target.asBuff = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공속버프 +" + targetNodeUI.target.asBuff;
                text2.text = null;
            }
            if (this.name == "buffas5")
            {
                targetNodeUI.target.asBuff = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공속버프 +" + targetNodeUI.target.asBuff;
                text2.text = null;
            }
            if (this.name == "buffas6")
            {
                targetNodeUI.target.asBuff = 0.05f;
                targetNodeUI.target.cost = 20;
                text1.text = "공속버프 +" + targetNodeUI.target.asBuff;
                text2.text = null;
            }
            if (this.name == "range1")
            {
                targetNodeUI.target.range = 1.5f;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "range2")
            {
                targetNodeUI.target.range = 1.5f;
                targetNodeUI.target.cost = 20;
                text1.text = "사거리 +" + targetNodeUI.target.range;
                text2.text = null;
            }
            if (this.name == "slowamount1")
            {
                targetNodeUI.target.slowAmount = 0.07f;
                targetNodeUI.target.cost = 20;
                text1.text = "감속강도 +" + targetNodeUI.target.slowAmount;
                text2.text = null;
            }
            if (this.name == "slowamount2")
            {
                targetNodeUI.target.slowAmount = 0.07f;
                targetNodeUI.target.cost = 20;
                text1.text = "감속강도 +" + targetNodeUI.target.slowAmount;
                text2.text = null;
            }
            if (this.name == "slowamount3")
            {
                targetNodeUI.target.slowAmount = 0.07f;
                targetNodeUI.target.cost = 20;
                text1.text = "감속강도 +" + targetNodeUI.target.slowAmount;
                text2.text = null;
            }
            if (this.name == "normal6")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "normal7")
            {
                targetNodeUI.target.supporterDmg = 2;
                targetNodeUI.target.cost = 20;
                text1.text = "공격력 +" + targetNodeUI.target.supporterDmg;
                text2.text = null;
            }
            if (this.name == "buffer")
            {
                targetNodeUI.target.cost = 40;
                text1.text = "버퍼로 전직";
                text2.text = null;
            }
            if (this.name == "debuffer")
            {
                targetNodeUI.target.cost = 40;
                text1.text = "디버퍼로 전직";
                text2.text = null;
            }
        }
        #endregion

        price.text = targetNodeUI.target.cost + " G";
    }

    public void MouseExit()
    {
        panel.SetActive(false);

        targetNodeUI.target.cost = 0;
    }
        
}
