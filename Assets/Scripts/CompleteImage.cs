﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompleteImage : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //Sprite screenshot = Resources.Load<Sprite>("suit_life_meter_2");
        Rect rect = new Rect(0, 0, 1280, 720);
        Image image = GetComponent<Image>();
        image.sprite = Sprite.Create(TurretCamera.screenshotTex, rect, new Vector2(0.5f, 0.5f));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
