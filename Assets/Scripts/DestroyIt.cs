﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyIt : MonoBehaviour {
    
    void Update()
    {
        if (!this.GetComponent<ParticleSystem>().isPlaying)
            Destroy(this.gameObject);
    }
	
}
