﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour {

    public static int EnemiesAlive = 0;

    public Wave[] waves;

    public Transform enemyPrefab;

    public Transform spawnPoint;

    public float timeBetweenWaves = 5f;
    private float countdown = 10f;

    public Text waveCountdownText;
    public Text remainEnemies;

    public GameManager gameManager;
    public GameObject skipButton;

    public GameObject startArrow;
    public GameObject endArrow;

    private int waveIndex = 0;
       
    void Update () {

        //remainEnemies.text = EnemiesAlive.ToString();

        if (EnemiesAlive > 0)
        {            
            return;
        }

        if (waveIndex == waves.Length)
        {
            gameManager.WinLevel();
            this.enabled = false;
        }

        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
            return;
        }

        if (!TutorialManager.isTutorialInProgress)
            countdown -= Time.deltaTime;

        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

        waveCountdownText.text = string.Format("{0:00.00}", countdown);

        if (countdown > 0)
        {
            skipButton.SetActive(false);
        }

        if (waveIndex == 0 && countdown > 0)
        {
            startArrow.SetActive(true);
            endArrow.SetActive(true);
        }
        else
        {
            startArrow.SetActive(false);
            endArrow.SetActive(false);
        }
    }

    IEnumerator SpawnWave()
    {
        //skipButton.SetActive(true);

        PlayerStats.Rounds++;

        Wave wave = waves[waveIndex];

        EnemiesAlive += wave.count + wave.count2;

        for (int i = 0; i < wave.count; i++)
        {
            SpawnEnemy(wave.enemy);
            yield return new WaitForSeconds(1f / wave.rate);
        }

        yield return new WaitForSeconds(wave.term);

        for (int i = 0; i < wave.count2; i++)
        {
            SpawnEnemy(wave.enemy2);
            yield return new WaitForSeconds(1f / wave.rate);
        }

        waveIndex++;
        
    }

    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
    }

    public void SkipButton()
    {
        StartCoroutine(SpawnWave());
        countdown = timeBetweenWaves;
    }
}
