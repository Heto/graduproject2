﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCamera : MonoBehaviour {

    public Camera mainCamera;
    public Camera turretCamera;

    private GameObject[] turretList;
	

    public void ClickButton()
    {
        if (mainCamera.enabled && GameObject.FindGameObjectWithTag("Turret"))
        {
            turretList = GameObject.FindGameObjectsWithTag("Turret");
            turretCamera = turretList[Random.Range(0, turretList.Length)].GetComponentInChildren<Camera>();

            turretCamera.enabled = true;
            mainCamera.enabled = false;
        }
        else if (!mainCamera.enabled && GameObject.FindGameObjectWithTag("Turret"))
        {
            turretCamera.enabled = false;
            mainCamera.enabled = true;
        }

    }
}
