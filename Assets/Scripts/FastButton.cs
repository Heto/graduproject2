﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FastButton : MonoBehaviour {

    public GameObject legoObj;
    public GameObject carObj;
    public Image fasterBG;

    void Update()
    {
        if (Time.timeScale == 2f)
            fasterBG.color = Color.yellow;
        else
            fasterBG.color = Color.white;
    }

    public void FastButtonClick()
    {
        if (Time.timeScale == 1f)
            Time.timeScale = 2f;
        else
            Time.timeScale = 1f;
    }

    public void LegoButtonClick()
    {
        if (PlayerStats.Legos > 0)
        {
            Instantiate(legoObj, new Vector3(-100, 0, -100), Quaternion.identity);
            PlayerStats.Legos--;
        }        
    }

    public void CarButtonClick()
    {
        if (PlayerStats.Cars > 0)
        {
            Instantiate(carObj, new Vector3(-100, 0, -100), Quaternion.identity);
            PlayerStats.Cars--;
        }
    }

}
