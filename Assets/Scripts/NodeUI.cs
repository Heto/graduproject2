﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeUI : MonoBehaviour {

    public GameObject ui;

    public Text upgradeCost;
    public Button upgradeButton;

    public Text sellAmount;

    public Node target;
    
    public void SetTarget(Node _target)
    {
        target = _target;

        transform.position = target.GetBuildPosition();
        
        if (!target.isUpgraded)
        {
            //upgradeCost.text = "$" + target.turretBlueprint.upgradeCost;
            upgradeButton.interactable = true;
        } else
        {
            //upgradeCost.text = "Done";
            upgradeButton.interactable = false;
        }

        //sellAmount.text = "$" + target.turretBlueprint.GetSellAmount();

        ui.SetActive(true);
    }

    public void Hide()
    {
        ui.SetActive(false);
    }

    public void Upgrade()
    {
        //target.UpgradeTurret();
        //BuildManager.instance.DeselectNode();
    }

    public void Sell()
    {
        target.SellTurret();
        BuildManager.instance.DeselectNode();
    }

    public void ExitButton()
    {
        if (TutorialManager.tutorialProgress == 6)
            return;

        BuildManager.instance.DeselectNode();
        if (TutorialManager.tutorialProgress == 7)
            TutorialManager.tutorialProgress = 8;
    }

    #region Archer
    public void Normal0Click()
    {
        target.Normal0();
        if (TutorialManager.tutorialProgress == 6)
            TutorialManager.tutorialProgress = 7;
    }
    public void Normal1Click()
    {
        target.Normal1();
    }
    public void Normal2Click()
    {
        target.Normal2();
    }
    public void Normal3Click()
    {
        target.Normal3();
    }
    public void Range1Click()
    {
        target.Range1();
    }
    public void Range2Click()
    {
        target.Range2();
    }
    public void Range3Click()
    {
        target.Range3();
    }
    public void Normal4Click()
    {
        target.Normal4();
    }
    public void Crichan1Click()
    {
        target.Crichan1();
    }
    public void Crichan2Click()
    {
        target.Crichan2();
    }
    public void Crichan3Click()
    {
        target.Crichan3();
    }
    public void Cridmg1Click()
    {
        target.Cridmg1();
    }
    public void Cridmg2Click()
    {
        target.Cridmg2();
    }
    public void CriSquClick()
    {
        target.CriSqu();
    }
    public void Normal5Click()
    {
        target.Normal5();
    }
    public void As1Click()
    {
        target.As1();
    }
    public void As2Click()
    {
        target.As2();
    }
    public void As3Click()
    {
        target.As3();
    }
    public void AsSquClick()
    {
        target.AsSqu();
    }
    public void Dmg1Click()
    {
        target.Dmg1();
    }
    public void Dmg2Click()
    {
        target.Dmg2();
    }
    public void Dmg3Click()
    {
        target.Dmg3();
    }
    public void DmgSquClick()
    {
        target.DmgSqu();
    }
    public void Normal6Click()
    {
        target.Normal6();
    }
    public void Normal7Click()
    {
        target.Normal7();
    }
    public void Runan1Click()
    {
        target.Runan1();
    }
    public void Runan2Click()
    {
        target.Runan2();
    }
    public void RangerUpgrade()
    {
        target.RangerUpgrade();
    }
    public void ExecutionerUpgrade()
    {
        target.ExecutionerUpgrade();
    }
    #endregion

    #region Magician
    public void Normal0_2()
    {
        target.Normal0_2();
        if (TutorialManager.tutorialProgress == 6)
            TutorialManager.tutorialProgress = 7;
    }
    public void Normal1_2()
    {
        target.Normal1_2();
    }
    public void Radius1_2()
    {
        target.Radius1_2();
    }
    public void Radius2_2()
    {
        target.Radius2_2();
    }
    public void Radius3_2()
    {
        target.Radius3_2();
    }
    public void Radius4_2()
    {
        target.Radius4_2();
    }
    public void Mpgain1_2()
    {
        target.Mpgain1_2();
    }
    public void Mpgain2_2()
    {
        target.Mpgain2_2();
    }
    public void Mpgain3_2()
    {
        target.Mpgain3_2();
    }
    public void Mpgain4_2()
    {
        target.Mpgain4_2();
    }
    public void As1_2()
    {
        target.As1_2();
    }
    public void As2_2()
    {
        target.As2_2();
    }
    public void As3_2()
    {
        target.As3_2();
    }
    public void AllSqu_2()
    {
        target.AllSqu_2();
    }
    public void Range1_2()
    {
        target.Range1_2();
    }
    public void Range2_2()
    {
        target.Range2_2();
    }
    public void Range3_2()
    {
        target.Range3_2();
    }
    public void Range4_2()
    {
        target.Range4_2();
    }
    public void Skill1_2()
    {
        target.Skill1_2();
    }
    public void Skill2_2()
    {
        target.Skill2_2();
    }
    public void Skill3_2()
    {
        target.Skill3_2();
    }
    public void Skill4_2()
    {
        target.Skill4_2();
    }
    public void Skill5_2()
    {
        target.Skill5_2();
    }
    public void Normal2_2()
    {
        target.Normal2_2();
    }
    public void Normal3_2()
    {
        target.Normal3_2();
    }
    public void Normal4_2()
    {
        target.Normal4_2();
    }
    public void Normal5_2()
    {
        target.Normal5_2();
    }
    public void SorcererUpgrade()
    {
        target.SorcererUpgrade();
    }
    public void SummonerUpgrade()
    {
        target.SummonerUpgrade();
    }
    #endregion

    #region Melee
    public void Normal0_0()
    {
        target.Normal0_0();
        if (TutorialManager.tutorialProgress == 6)
            TutorialManager.tutorialProgress = 7;
    }
    public void Normal1_0()
    {
        target.Normal1_0();
    }
    public void Normal9_0()
    {
        target.Normal9_0();
    }
    public void Normal10_0()
    {
        target.Normal10_0();
    }
    public void Stunnum1_0()
    {
        target.Stunnum1_0();
    }
    public void Normal2_0()
    {
        target.Normal2_0();
    }
    public void Range1_0()
    {
        target.Range1_0();
    }
    public void Stuntime1_0()
    {
        target.Stuntime1_0();
    }
    public void Stuntime2_0()
    {
        target.Stuntime2_0();
    }
    public void As1_0()
    {
        target.As1_0();
    }
    public void As2_0()
    {
        target.As2_0();
    }
    public void As3_0()
    {
        target.As3_0();
    }
    public void Normal3_0()
    {
        target.Normal3_0();
    }
    public void Normal4_0()
    {
        target.Normal4_0();
    }
    public void Normal5_0()
    {
        target.Normal5_0();
    }
    public void Range2_0()
    {
        target.Range2_0();
    }
    public void Stuntime3_0()
    {
        target.Stuntime3_0();
    }
    public void Stuntime4_0()
    {
        target.Stuntime4_0();
    }
    public void StuntimeSqu_0()
    {
        target.StuntimeSqu_0();
    }
    public void Stunnum2_0()
    {
        target.Stunnum2_0();
    }
    public void As4_0()
    {
        target.As4_0();
    }
    public void As5_0()
    {
        target.As5_0();
    }
    public void Dmg1_0()
    {
        target.Dmg1_0();
    }
    public void Dmg2_0()
    {
        target.Dmg2_0();
    }
    public void DmgSqu_0()
    {
        target.DmgSqu_0();
    }
    public void Normal6_0()
    {
        target.Normal6_0();
    }
    public void Normal7_0()
    {
        target.Normal7_0();
    }
    public void Normal11_0()
    {
        target.Normal11_0();
    }
    public void Normal12_0()
    {
        target.Normal12_0();
    }
    public void Normal13_0()
    {
        target.Normal13_0();
    }
    public void Stunnum3_0()
    {
        target.Stunnum3_0();
    }
    public void Normal8_0()
    {
        target.Normal8_0();
    }
    public void KnightUpgrade()
    {
        target.KnightUpgrade();
    }
    public void BerserkerUpgrade()
    {
        target.BerserkerUpgrade();
    }
    #endregion

    #region Supporter
    public void Normal0_3()
    {
        target.Normal0_3();
        if (TutorialManager.tutorialProgress == 6)
            TutorialManager.tutorialProgress = 7;
    }
    public void Normal1_3()
    {
        target.Normal1_3();
    }
    public void Normal2_3()
    {
        target.Normal2_3();
    }
    public void Buffdmg1_3()
    {
        target.Buffdmg1_3();
    }
    public void Buffdmg2_3()
    {
        target.Buffdmg2_3();
    }
    public void Buffdmg3_3()
    {
        target.Buffdmg3_3();
    }
    public void Buffas1_3()
    {
        target.Buffas1_3();
    }
    public void Buffas2_3()
    {
        target.Buffas2_3();
    }
    public void Buffas3_3()
    {
        target.Buffas3_3();
    }
    public void Skill1_3()
    {
        target.Skill1_3();
    }
    public void Skill2_3()
    {
        target.Skill2_3();
    }
    public void Normal3_3()
    {
        target.Normal3_3();
    }
    public void Normal4_3()
    {
        target.Normal4_3();
    }
    public void Normal5_3()
    {
        target.Normal5_3();
    }
    public void Buffdmg4_3()
    {
        target.Buffdmg4_3();
    }
    public void Buffdmg5_3()
    {
        target.Buffdmg5_3();
    }
    public void Buffas4_3()
    {
        target.Buffas4_3();
    }
    public void Buffas5_3()
    {
        target.Buffas5_3();
    }
    public void Buffas6_3()
    {
        target.Buffas6_3();
    }
    public void Range1_3()
    {
        target.Range1_3();
    }
    public void Range2_3()
    {
        target.Range2_3();
    }
    public void Slowamount1_3()
    {
        target.Slowamount1_3();
    }
    public void Slowamount2_3()
    {
        target.Slowamount2_3();
    }
    public void Slowamount3_3()
    {
        target.Slowamount3_3();
    }
    public void Normal6_3()
    {
        target.Normal6_3();
    }
    public void Normal7_3()
    {
        target.Normal7_3();
    }
    public void Normal8_3()
    {
        target.Normal8_3();
    }
    public void Normal9_3()
    {
        target.Normal9_3();
    }
    public void Range3_3()
    {
        target.Range3_3();
    }
    public void Normal10_3()
    {
        target.Normal10_3();
    }
    public void Skill3_3()
    {
        target.Skill3_3();
    }
    public void Skill4_3()
    {
        target.Skill4_3();
    }
    public void Skill5_3()
    {
        target.Skill5_3();
    }
    public void BufferUpgrade()
    {
        target.BufferUpgrade();
    }
    public void DebufferUpgrade()
    {
        target.DebufferUpgrade();
    }
    #endregion
}
