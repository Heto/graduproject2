﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfAnim : MonoBehaviour {

    public float damage = 0f;
    public AudioSource sound;

    void Start()
    {
        sound = GetComponent<AudioSource>();
    }

    public void SoundPlay()
    {
        sound.Play();
    }

    public void MeleeStrike()
    {
        Turret t = GetComponentInParent<Turret>();

        if (t.buffed)
            damage = t.buffedTurretDmg;
        else
            damage = t.normalTurretDmg;

        if (t.berserkerSkill)
        {
            t.targetEnemy.TakeDamage(damage * 2);
            t.BerserkerSkillUse();
        }
        else
        {
            t.targetEnemy.TakeDamage(damage);
        }
    }

    public void ToIdle()
    {
        Animator anim = GetComponent<Animator>();
        anim.SetTrigger("ToIdle");
    }
}
