﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class PauseMenu : MonoBehaviour {

    public GameObject ui;
    public GameObject helpUI;

    public Slider MasterSlider;
    public Slider BGMSlider;
    public Slider SFXSlider;

    public string menuSceneName = "MainMenu";

    public SceneFader sceneFader;

    public AudioMixer masterMixer;

    void Start()
    {
        MasterSlider.value = PlayerPrefs.GetFloat("MasterVolume");
        BGMSlider.value = PlayerPrefs.GetFloat("BGMVolume");
        SFXSlider.value = PlayerPrefs.GetFloat("SFXVolume");
    }

    // Update is called once per frame
    void Update () {

		if (Input.GetKeyDown(KeyCode.Escape))
        {
            Toggle();
        }
	}

    public void Toggle()
    {
        if (GameManager.GameIsOver)
            return;

        ui.SetActive(!ui.activeSelf);
        //slider.value = AudioListener.volume;

        if (ui.activeSelf)
        {
            Time.timeScale = 0f;
        } else
        {
            Time.timeScale = 1f;
        }
    }

    public void Retry()
    {
        Toggle();
        sceneFader.FadeTo(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        Toggle();
        sceneFader.FadeTo(menuSceneName);
    }

    public void Help()
    {
        helpUI.SetActive(true);
    }
    public void HelpUIExit()
    {
        Toggle();
        helpUI.SetActive(false);
    }

    //public void VolumeSlider(float volume)
    //{
    //    slider.value = AudioListener.volume;
    //    AudioListener.volume = volume;
    //}

    public void SetMasterLevel(float masterLvl)
    {
        masterMixer.SetFloat("MasterVolume", masterLvl);
        PlayerPrefs.SetFloat("MasterVolume", masterLvl);
    }
    public void SetSFXLevel(float sfxLvl)
    {
        masterMixer.SetFloat("SFXVolume", sfxLvl);
        PlayerPrefs.SetFloat("SFXVolume", sfxLvl);
    }
    public void SetBGMLevel(float bgmLvl)
    {
        masterMixer.SetFloat("BGMVolume", bgmLvl);
        PlayerPrefs.SetFloat("BGMVolume", bgmLvl);
    }
}
