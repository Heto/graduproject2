﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteLevel : MonoBehaviour {

    public string menuSceneName = "MainMenu";

    public string nextLevel = "MainLevel2";
    public int levelToUnlock = 2;
   
    public SceneFader sceneFader;

    public AudioSource audiosource;
    public AudioClip bgm;
    public AudioClip stampClip;

    void Start()
    {
        audiosource.PlayOneShot(bgm);
    }

    public void Continue()
    {
        Time.timeScale = 1f;
        sceneFader.FadeTo(nextLevel);
    }

	public void Menu()
    {
        Time.timeScale = 1f;
        sceneFader.FadeTo(menuSceneName);
    }

    public void Ending()
    {
        Time.timeScale = 1f;
        sceneFader.FadeTo("Ending");
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void StampSoundPlay()
    {
        audiosource.PlayOneShot(stampClip);
    }
}
