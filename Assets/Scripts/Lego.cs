﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lego : MonoBehaviour {

    public bool isActive = false;
    public GameObject legoEffect;

    void Update()
    {
        if (!isActive)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                Vector3 objectHit = hit.point;
                objectHit.y = 1.6f;
                transform.position = objectHit;
            }
        }        

        if (Input.GetMouseButtonDown(0))
        {
            if (!isActive)
                isActive = true;
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (!isActive)
            {
                PlayerStats.Legos++;
                Destroy(gameObject);
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (isActive && col.tag == "Enemy")
        {
            Enemy e = col.GetComponent<Enemy>();
            e.TakeLegoDamage(300);
            Instantiate(legoEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
