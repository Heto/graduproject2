﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MainMenu : MonoBehaviour {
        
    public string levelToLoad = "Intro";
    
    public SceneFader sceneFader;
    public GameObject optionMenu;
    public GameObject creditMenu;
    public GameObject creditContents;
    public Transform startPosition;
    public GameObject nodataPanel;

    public Slider MasterSlider;
    public Slider BGMSlider;
    public Slider SFXSlider;

    public AudioMixer masterMixer;

    void Start()
    {
        MasterSlider.value = PlayerPrefs.GetFloat("MasterVolume");
        BGMSlider.value = PlayerPrefs.GetFloat("BGMVolume");
        SFXSlider.value = PlayerPrefs.GetFloat("SFXVolume");
    }

    public void Play()
    {
        sceneFader.FadeTo(levelToLoad);
    }

    public void Load()
    {
        if (PlayerPrefs.HasKey("Save"))
        {
            sceneFader.FadeTo("MainLevel2");
        }
        else
        {
            nodataPanel.SetActive(true);
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.U))
            PlayerPrefs.DeleteKey("Save");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Option()
    {
        optionMenu.SetActive(true);
        //slider.value = AudioListener.volume;
    }
    //public void VolumeSlider(float volume)
    //{
    //    slider.value = AudioListener.volume;
    //    AudioListener.volume = volume;
    //}
    public void SetMasterLevel(float masterLvl)
    {
        masterMixer.SetFloat("MasterVolume", masterLvl);
        PlayerPrefs.SetFloat("MasterVolume", masterLvl);
    }
    public void SetSFXLevel(float sfxLvl)
    {
        masterMixer.SetFloat("SFXVolume", sfxLvl);
        PlayerPrefs.SetFloat("SFXVolume", sfxLvl);
    }
    public void SetBGMLevel(float bgmLvl)
    {
        masterMixer.SetFloat("BGMVolume", bgmLvl);
        PlayerPrefs.SetFloat("BGMVolume", bgmLvl);
    }
    public void OptionBack()
    {
        optionMenu.SetActive(false);
    }
    public void Credit()
    {
        creditMenu.SetActive(true);
    }
    public void CreditBack()
    {
        creditContents.transform.position = startPosition.transform.position;
        creditMenu.SetActive(false);
    }
}
