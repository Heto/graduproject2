﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour {

    public TurretBlueprint standardTurret;
    public TurretBlueprint missileLauncher;
    public TurretBlueprint laserBeamer;
    public TurretBlueprint melee;

    public GameObject turretExplainPanel;
    public GameObject meleeText;
    public GameObject archerText;
    public GameObject magicianText;
    public GameObject supporterText;

    BuildManager buildManager;

    void Start()
    {
        buildManager = BuildManager.instance;
    }

    public void SelectStandardTurret()
    {
        buildManager.SelectTurretToBuild(standardTurret);
    }

    public void SelectMissileLauncher()
    {
        buildManager.SelectTurretToBuild(missileLauncher);
    }

    public void SelectLaserBeamer()
    {
        buildManager.SelectTurretToBuild(laserBeamer);
    }

    public void SelectMelee()
    {
        buildManager.SelectTurretToBuild(melee);
    }

    public void MeleeMouseEnter()
    {
        turretExplainPanel.SetActive(true);
        meleeText.SetActive(true);
        archerText.SetActive(false);
        magicianText.SetActive(false);
        supporterText.SetActive(false);
    }
    public void ArcherMouseEnter()
    {
        turretExplainPanel.SetActive(true);
        meleeText.SetActive(false);
        archerText.SetActive(true);
        magicianText.SetActive(false);
        supporterText.SetActive(false);
    }
    public void MagicianMouseEnter()
    {
        turretExplainPanel.SetActive(true);
        meleeText.SetActive(false);
        archerText.SetActive(false);
        magicianText.SetActive(true);
        supporterText.SetActive(false);
    }
    public void SupporterMouseEnter()
    {
        turretExplainPanel.SetActive(true);
        meleeText.SetActive(false);
        archerText.SetActive(false);
        magicianText.SetActive(false);
        supporterText.SetActive(true);
    }

    public void MouseExitt()
    {
        turretExplainPanel.SetActive(false);
    }
}
