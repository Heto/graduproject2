﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SorcererSkill : MonoBehaviour {

    public Turret turret;
    public Enemy target;
    public ParticleSystem parts;
    float totalDuration;
	// Use this for initialization
	void Start () {
        parts = GetComponent<ParticleSystem>();
        totalDuration = parts.main.duration + parts.main.startLifetimeMultiplier - 0.6f;

        StartCoroutine(Damage());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Damage()
    {
        yield return new WaitForSeconds(totalDuration);

        target.TakeDamage(turret.skillDmg);
        Destroy(gameObject);
    }

    public void SkillDamage()
    {
        target.TakeDamage(turret.skillDmg);
        Destroy(gameObject);

    }
}
