﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesUI : MonoBehaviour {

    public Text livesText;
    public Text waveText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        livesText.text = PlayerStats.Lives.ToString() + " LIVES";
        waveText.text = PlayerStats.Rounds.ToString() + " WAVE";
	}
}
