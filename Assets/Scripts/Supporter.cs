﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Supporter : MonoBehaviour {

    public float radius = 0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Explode();

    }

    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Turret")
            {
                Turret t = collider.GetComponent<Turret>();
                t.GetBuff();
                Debug.Log("123");
            }
        }
    }
}
