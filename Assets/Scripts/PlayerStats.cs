﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    public static int Money;
    public int startMoney = 400;

    public static int Lives;
    public int startLives = 20;

    public static int Legos;
    public int startLegos = 4;

    public static int Cars;
    public int startCars = 4;

    public static int Rounds;

    void Start()
    {
        Money = startMoney;
        Lives = startLives;
        Legos = startLegos;
        Cars = startCars;

        Rounds = 0;
    }

    void Update()
    {
        Lives = (int)Mathf.Clamp(Lives, 0, Mathf.Infinity);
    }


}
