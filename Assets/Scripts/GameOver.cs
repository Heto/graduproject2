﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
    
    public string menuSceneName = "MainMenu";

    public SceneFader sceneFader;

    public AudioSource audiosource;
    public AudioClip bgm;
    public AudioClip stampClip;

    void Start()
    {
        audiosource.PlayOneShot(bgm);
    }
       
    public void Retry()
    {
        Time.timeScale = 1f;
        sceneFader.FadeTo(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        Time.timeScale = 1f;
        sceneFader.FadeTo(menuSceneName);
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void StampSoundPlay()
    {
        audiosource.PlayOneShot(stampClip);
    }
}
