﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeadEffect : MonoBehaviour {

    private GameObject enemy;

    void Start()
    {
        enemy = gameObject.GetComponentInParent<Enemy>().gameObject;
    }

    public void Dead()
    {
        Destroy(enemy);
    }
}
