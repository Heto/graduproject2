﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour {

    public GameObject[] tutorialPanel;
    public static float tutorialProgress = 1;
    public static bool isTutorialInProgress = true;
    public bool giveUpgradeMoney = true;
    
    // Update is called once per frame
    void Update() {

        if (isTutorialInProgress)
        {
            if (tutorialProgress == 1)
            {
                tutorialPanel[0].SetActive(true);
            }
            else if (tutorialProgress == 2)
            {
                tutorialPanel[0].SetActive(false);
                tutorialPanel[1].SetActive(true);
            }
            else if (tutorialProgress == 3)
            {
                tutorialPanel[1].SetActive(false);
                tutorialPanel[2].SetActive(true);
            }
            else if (tutorialProgress == 4)
            {
                tutorialPanel[2].SetActive(false);
                tutorialPanel[3].SetActive(true);
            }
            else if (tutorialProgress == 5)
            {
                tutorialPanel[3].SetActive(false);
                tutorialPanel[4].SetActive(true);
            }
            else if (tutorialProgress == 6)
            {
                tutorialPanel[4].SetActive(false);
                tutorialPanel[5].SetActive(true);
                if (giveUpgradeMoney)
                {
                    PlayerStats.Money += 20;
                    giveUpgradeMoney = false;
                }                
            }
            else if (tutorialProgress == 7)
            {
                tutorialPanel[5].SetActive(false);
                tutorialPanel[6].SetActive(true);
            }
            else if (tutorialProgress == 8)
            {
                tutorialPanel[6].SetActive(false);
                tutorialPanel[7].SetActive(true);
            }
            else if (tutorialProgress == 9)
            {
                tutorialPanel[7].SetActive(false);
                tutorialPanel[8].SetActive(true);
            }
            else if (tutorialProgress == 10)
            {
                tutorialPanel[8].SetActive(false);
                isTutorialInProgress = false;
            }

        }
        
    }

    public void PressPanel()
    {
        tutorialProgress++;
    }
}
