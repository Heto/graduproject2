﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LegoUI : MonoBehaviour {

    public Text legoText;
    public Text carText;

    void Update()
    {
        legoText.text = PlayerStats.Legos.ToString();
        carText.text = PlayerStats.Cars.ToString();
    }
}
