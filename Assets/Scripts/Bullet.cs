﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private Transform target;
    public Turret turret;
    public bool isMagic;
    public bool isPoison;
    public bool isTrueDamage = false;

    public float speed = 70f;

    public float damage = 50;

    private float criticalIndex;
    public float criticalChance;
    public float criticalDmg;
    public float explosionRadius = 0f;
    public float mpGainAmount;
    public float slowAmount = 0f;

    public GameObject impactEffect;

	public void Seek (Transform _target)
    {
        target = _target;
    }
	
	// Update is called once per frame
	void Update () {
		if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if(dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);

	}

    void HitTarget()
    {
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5f);

        if (explosionRadius > 0f)
        {
            Explode();
        } else
        {
            Damage(target);
        }

        Destroy(gameObject);
    }

    void Explode ()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                Damage(collider.transform);

                if (slowAmount > 0)
                {
                    Enemy e = collider.GetComponent<Enemy>();
                    e.SlowSkill(slowAmount);
                }
            }
        }
    }

    void Damage(Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();

        if (e != null)
        {            
            criticalIndex = Random.value;
            if (criticalChance < criticalIndex)
            {
                if (isTrueDamage)
                {
                    e.TakeTrueDamage(damage);
                }                    
                else
                {
                    e.TakeDamage(damage);
                }                    
            } else
            {
                if (isTrueDamage)
                    e.TakeTrueDamage(damage * criticalDmg);
                else
                    e.TakeDamage(damage * criticalDmg);
            }
                

            if (isPoison)
            {
                if (e.isPoisoning)
                    e.poisonStack++;
                else
                {
                    e.poisonStack++;
                    e.Poison();
                }
            }

            if (turret.typeOfTurret == 2 && !isMagic)
                turret.GainMp(mpGainAmount);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
